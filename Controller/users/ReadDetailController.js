import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createglucose_check,
  createusers,
  createusers_child,
  findOne_glucose_check,
  findOne_users,
  findOne_users_child,
  readAllglucose_check,
  readAllusers_child,
  updateusers,
} from "../../Services/users/UsersRepository.js";
import { Op } from "sequelize";
import {
  findOne_master_viva,
  updatemaster_viva,
} from "../../Services/master_viva/MasterVivaRepository.js";
import { decrypt } from "../../Helper/Helper.js";
import moment from "moment";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Ditampilkan", 422, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      function calculateAge(born) {
        const birthDate = new Date(born);
        const currentDate = new Date();

        const yearsDiff = currentDate.getFullYear() - birthDate.getFullYear();
        const monthsDiff = currentDate.getMonth() - birthDate.getMonth();

        if (
          monthsDiff < 0 ||
          (monthsDiff === 0 && currentDate.getDate() < birthDate.getDate())
        ) {
          return yearsDiff - 1;
        }

        return yearsDiff;
      }

      if (!req.query.code) {
        return error_handling(
          "Data Gagal Ditampilkan",
          400,
          [{ field: "code", message: "code harus dikirim" }],
          res
        );
      }
      let getData = await findOne_glucose_check({
        where: { code: req.query.code },
      });
      let getPatient = await findOne_users_child({
        where: { id: getData.users_child_id },
      });
      let result = {
        code: getData.code,
        name: getData.name,
        gender: getPatient.gender,
        born: getPatient.born,
        flag: getData.flag,
        blood_sugar_value: getData.blood_sugar_value,
        age: calculateAge(getPatient.born),
      };
      return success("Data Berhasil Ditampilkan", 200, result, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling(
      "Data Gagal Ditampilkan",
      403,
      { message: error.message },
      res
    );
  }
}
