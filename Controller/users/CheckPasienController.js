import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  findOne_users,
  readAllglucose_check,
  readAllusers,
  readAllusers_child,
  readusersFirst,
} from "../../Services/users/UsersRepository.js";
import { Op } from "sequelize";

export default async function get(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling(
        "Read data gagal ditampilkan",
        422,
        errors.array(),
        res
      );
    } else {
      const users = req.query?.users_login || null;
      let tax = await findOne_users({ where: { name: users } });
      let pasien = await readAllusers_child({
        where: {
          users_id: tax.id,
        },
      });
      let listIdPasien = [];
      pasien.forEach((element) => {
        listIdPasien.push(element.id);
      });
      let pasienGlucose = await readAllglucose_check({
        where: {
          users_child_id: { [Op.in]: listIdPasien },
          code_viva: { [Op.and]: [{ [Op.not]: null }, { [Op.not]: "" }] },
        },
      });
      let result = true;
      //   console.log(pasien.length, tax.isTrial, pasienGlucose.length);
      if (
        (pasien.length >= 1 && tax.isTrial) ||
        (pasien.length >= 1 && pasienGlucose.length == 0)
      ) {
        result = false;
      }
      return success("Read data berhasil", 200, result, res);
    }
  } catch (error) {
    console.error(error);
    return error_handling(
      "Read data gagal ditampilkan",
      403,
      { message: error.message },
      res
    );
  }
}
