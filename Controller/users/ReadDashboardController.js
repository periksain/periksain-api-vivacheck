import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  findOne_users,
  readAllglucose_check,
  readusersFirst,
  readusersFirstNoGlucose,
  readusersFirstOnly,
} from "../../Services/users/UsersRepository.js";
import { Op } from "sequelize";
import { decrypt } from "../../Helper/Helper.js";

export default async function get(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling(
        "Read data gagal ditampilkan",
        422,
        errors.array(),
        res
      );
    } else {
      function getDateTime(date) {
        // var date = new Date();
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return (
          year + "-" + month + "-" + day
          // + " " + hour + ":" + min + ":" + sec
        );
      }
      function getTime(date) {
        // var date = new Date();
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return (
          //   year + "-" + month + "-" + day
          //   + " " +
          hour + ":" + min
          //   + ":" + sec
        );
      }

      const temp_search = req.query?.users_child_code || null;
      const users = req.query?.users_login || null;
      let start = new Date(req.query?.start || null);
      let akhir = new Date(req.query?.end || null);
      let search;
      if (temp_search) {
        let tempsearch = decrypt("vivachek", temp_search);
        search = tempsearch.replace("child ", "");
      }
      let check = await findOne_users({ where: { name: users } });
      let tax = await readusersFirst(search, users, start, akhir);
      let result;

      result = tax?.users_children[0] || null;
      // console.log("result first ", result);
      if (result == null && check) {
        if (!temp_search) {
          return error_handling(
            "Read data gagal ditampilkan",
            403,
            { message: "Code users child harus dimasukkan" },
            res
          );
        }
        let list = await readusersFirstNoGlucose(search, users);
        // console.log(list);
        if (!list) {
          return error_handling(
            "Read data gagal ditampilkan",
            403,
            { message: "Users child id salah" },
            res
          );
        }
        let temp = list.users_children[0];
        // console.log('result atas ', temp.dataValues);
        let isi_glucose = await readAllglucose_check({
          where: {
            users_child_id: temp.id,
          },
        });
        let array = [];
        isi_glucose.forEach((element) => {
          array.push({ ...element.dataValues });
        });
        result = temp.dataValues;
        result.glucose_checks = array;
      }
      // console.log("result hasil", result);
      let list = [];
      let statistik;
      let list_catatan = [];
      let statistik_rendah = 0;
      let statistik_normal = 0;
      let statistik_tinggi = 0;
      let statistik_total_nilai = 0;
      let list_tanggal = [];
      result?.glucose_checks.forEach((element) => {
        list_catatan.push({
          code: element.code,
          tanggal: element.inspection_time_date,
          waktu: element.inspection_time_hour.substr(0, 5),
          jenis_pemeriksaan: element.checking_type,
          flag: element.flag,
          blood_sugar_value: element.blood_sugar_value,
        });
        list.push(element.blood_sugar_value);
        statistik_total_nilai += element.blood_sugar_value;
        if (element.flag == "Normal") {
          statistik_normal += 1;
          list_tanggal.push({
            tgl: element.inspection_time_date,
            rendah: 0,
            normal: 1,
            tinggi: 0,
          });
        } else if (element.flag == "Rendah") {
          statistik_rendah += 1;
          list_tanggal.push({
            tgl: element.inspection_time_date,
            rendah: 1,
            normal: 0,
            tinggi: 0,
          });
        } else {
          statistik_tinggi += 1;
          list_tanggal.push({
            tgl: element.inspection_time_date,
            rendah: 0,
            normal: 0,
            tinggi: 1,
          });
        }
      });
      statistik = {
        rendah: statistik_rendah,
        normal: statistik_normal,
        tinggi: statistik_tinggi,
        average: +(statistik_total_nilai / list.length).toFixed(1),
        minimal: Math.min(...list),
        maximal: Math.max(...list),
      };

      let resultListTanggal = Object.values(
        list_tanggal.reduce((acc, curr) => {
          const key = `${curr.tgl}`;
          if (acc[key]) {
            acc[key].rendah += curr.rendah;
            acc[key].normal += curr.normal;
            acc[key].tinggi += curr.tinggi;
          } else {
            acc[key] = { ...curr };
          }
          return acc;
        }, {})
      );

      function compareDates(date1, date2) {
        return new Date(date1) - new Date(date2);
      }

      // Urutkan array berdasarkan tanggal terkecil
      resultListTanggal.sort((a, b) => compareDates(a.tgl, b.tgl));

      function generateDateRangeWithValues(start, end, array) {
        const startDate = new Date(start);
        const endDate = new Date(end);

        const result = [];
        let currentDate = startDate;
        let arrayIndex = 0;

        while (currentDate <= endDate) {
          const tanggal = getDateTime(currentDate);

          if (
            arrayIndex < array.length &&
            currentDate.toISOString().slice(0, 10) === array[arrayIndex].tgl
          ) {
            result.push({
              tanggal,
              rendah: array[arrayIndex].rendah,
              normal: array[arrayIndex].normal,
              tinggi: array[arrayIndex].tinggi,
            });
            arrayIndex++;
          } else {
            result.push({ tanggal, rendah: 0, normal: 0, tinggi: 0 });
          }

          currentDate.setDate(currentDate.getDate() + 1);
        }

        return result;
      }

      let grafik_tanggal = generateDateRangeWithValues(
        start,
        akhir,
        resultListTanggal
      );

      // console.log(result);
      // console.log(statistik);
      // console.log(tax);
      if (
        result?.glucose_checks.length != 0 &&
        req.query?.start &&
        req.query?.end
      ) {
        // console.log('ATAS');
        if (tax == null) {
          result.statistik = [];
          result.grafik = [];
          result.catatan = [];
        } else {
          result.dataValues.statistik = tax != null ? statistik : [];
          result.dataValues.grafik = tax != null ? grafik_tanggal : [];
          result.dataValues.catatan = tax != null ? list_catatan : [];
        }
      } else {
        if (result) {
          // console.log('BAWAH');
          result.statistik = [];
          result.grafik = [];
          result.catatan = [];
        } else {
          result = null;
        }
      }

      return success("Read data berhasil", 200, result, res);
    }
  } catch (error) {
    console.error(error);
    return error_handling(
      "Read data gagal ditampilkan",
      403,
      { message: error.message },
      res
    );
  }
}
