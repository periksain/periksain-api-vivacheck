import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  findOne_users,
  readusersFirst,
} from "../../Services/users/UsersRepository.js";

export default async function get(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling(
        "Read data gagal ditampilkan",
        422,
        errors.array(),
        res
      );
    } else {
      const users = req.query?.users_login || null;
      let tax = await findOne_users({ where: { name: users } });
      let result = tax.isTrial;

      return success("Read data berhasil", 200, result, res);
    }
  } catch (error) {
    console.error(error);
    return error_handling(
      "Read data gagal ditampilkan",
      403,
      { message: error.message },
      res
    );
  }
}
