import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createusers,
  createusers_child,
  findOne_users,
  findOne_users_child,
  readAllglucose_check,
  readAllusers_child,
  updateusers_child,
} from "../../Services/users/UsersRepository.js";
import { crypt, decrypt } from "../../Helper/Helper.js";
import { Op } from "sequelize";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Disimpan", 403, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      let listError = [];
      if (!req.body?.users_login) {
        return error_handling(
          "Data Gagal Disimpan",
          400,
          [{ field: "users_login", message: "users_login harus dikirim" }],
          res
        );
      }

      // ambil users_login dari api/v1/customers/self/
      let checkData = await findOne_users({
        where: { name: req.body.users_login },
      });
      let createData;
      if (checkData) {
        let pasien = await readAllusers_child({
          where: {
            users_id: checkData.id,
          },
        });
        let cek_pasien = await findOne_users_child({
          where: {
            users_id: checkData.id,
            name: req.body.name,
          },
        });
        let listIdPasien = [];
        pasien.forEach((element) => {
          listIdPasien.push(element.id);
        });
        let pasienGlucose = await readAllglucose_check({
          where: {
            users_child_id: { [Op.in]: listIdPasien },
            code_viva: { [Op.and]: [{ [Op.not]: null }, { [Op.not]: "" }] },
          },
        });
        if (
          (pasien.length >= 1 && checkData.isTrial) ||
          (pasien.length >= 1 && pasienGlucose.length == 0)
        ) {
          return error_handling(
            "Data Gagal Disimpan",
            403,
            { message: "User trial tidak bisa mendaftar lebih dari 1 pasien" },
            res
          );
        }
        if (cek_pasien) {
          return error_handling(
            "Data Gagal Disimpan",
            403,
            { message: `Nama pasien ${cek_pasien.name} sudah terdaftar` },
            res
          );
        }
        let createPasien = await createusers_child({
          users_id: checkData.id,
          name: req.body.name,
          born: req.body.born,
          gender: req.body.gender,
          created_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        });
      } else {
        createData = await createusers({
          name: req.body.users_login,
          isTrial: true,
          created_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        });
        let createPasien = await createusers_child({
          users_id: createData.id,
          name: req.body.name,
          born: req.body.born,
          gender: req.body.gender,
          created_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        });
      }

      return success(
        "Data Berhasil Disimpan",
        201,
        checkData || createData,
        res
      );
    }
  } catch (error) {
    console.log(error);
    return error_handling("Data Gagal Disimpan", 403, {message:error.message}, res);
  }
}
