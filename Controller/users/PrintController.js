import pdfservice from "../../Helper/pdf-service.js";
import success from "../../Helper/Response/success.js";
import error from "../../Helper/Response/error.js";
import axios from "axios";
// import { setting_env } from "../../Helper/Helper.js";
import {
  findOne_glucose_check,
  findOne_users_child,
} from "../../Services/users/UsersRepository.js";
import { decrypt } from "../../Helper/Helper.js";

export default async function get(req, resreal, next) {
  try {
    console.table(req.query);
    let temp_id = decrypt("vivachek",req.query.code)
    let id = temp_id.replace("hasil ","")
    let data = await findOne_glucose_check({
      where: {
        id: id,
      },
    });

    let type = data.checking_type
    let hasil
    if (type == 'Gula Darah 2 Jam Pasca Makan') {
      hasil = ">70 mg/dL s/d <140 mg/dL"
    } else if (type == 'Gula Darah Puasa') {
      hasil = ">70 mg/dL s/d <126 mg/dL"
    } else if (type == 'Gula Darah Sewaktu') {
      hasil = ">70 mg/dL s/d <200 mg/dL"
    } else {
      hasil = "-"
    }

    let olahan = [
      data.inspection_time_date,
      hasil,
      data.blood_sugar_value,
      data.flag,
    ];

    let pasien = await findOne_users_child({
      where: {
        id: data.users_child_id,
      },
    });

    function calculateAge(born) {
      const birthDate = new Date(born);
      const currentDate = new Date();

      const yearsDiff = currentDate.getFullYear() - birthDate.getFullYear();
      const monthsDiff = currentDate.getMonth() - birthDate.getMonth();

      if (
        monthsDiff < 0 ||
        (monthsDiff === 0 && currentDate.getDate() < birthDate.getDate())
      ) {
        return yearsDiff - 1;
      }

      return yearsDiff;
    }

    let olahan_atas = {
      nama: pasien.name,
      tgl_lahir: pasien.born,
      umur: calculateAge(pasien.born),
      jenis_kelamin: pasien.gender,
      tanggal_periksa: data.inspection_time_date,
      waktu_periksa: data.inspection_time_hour.substr(0,5),
    }

    var hasilAkhir = await pdfservice.buildPDF(olahan, data.checking_type, olahan_atas);

    // console.log('hasilAkhir ', hasilAkhir);
    return success("Berhasil Print", 200, hasilAkhir, resreal);
  } catch (e) {
    console.error(e);
    return error("Read data gagal", 403, {message:e.message}, resreal);
  }
}
