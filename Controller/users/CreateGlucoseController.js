import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createglucose_check,
  createusers,
  createusers_child,
  findOne_glucose_check,
  findOne_users,
  findOne_users_child,
  readAllglucose_check,
  readAllusers,
  readAllusers_child,
  updateusers,
} from "../../Services/users/UsersRepository.js";
import { Op } from "sequelize";
import {
  findOne_master_viva,
  updatemaster_viva,
} from "../../Services/master_viva/MasterVivaRepository.js";
import { decrypt } from "../../Helper/Helper.js";
import moment from "moment";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Disimpan", 400, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      function calculateAge(born) {
        const birthDate = new Date(born);
        const currentDate = new Date();

        const yearsDiff = currentDate.getFullYear() - birthDate.getFullYear();
        const monthsDiff = currentDate.getMonth() - birthDate.getMonth();

        if (
          monthsDiff < 0 ||
          (monthsDiff === 0 && currentDate.getDate() < birthDate.getDate())
        ) {
          return yearsDiff - 1;
        }

        return yearsDiff;
      }

      function checkTimeDifference(waktu1, waktu_real) {
        const formatWaktu = "HH:mm:ss";
        const formatWaktuReal = "HH:mm";

        const waktu1Obj = moment(waktu1, formatWaktu);
        const waktuRealObj = moment(waktu_real, formatWaktuReal);

        const selisihJam = waktu1Obj.diff(waktuRealObj, "hours");
        // console.log(
        //   "🚀 ~ file: CreateGlucoseController.js:64 ~ checkTimeDifference ~ selisihJam:",
        //   selisihJam
        // );

        return selisihJam < 2 && selisihJam > -2;
      }

      function checkTime8Difference(waktu1, waktu_real) {
        const formatWaktu = "HH:mm:ss";
        const formatWaktuReal = "HH:mm";

        const waktu1Obj = moment(waktu1, formatWaktu);
        const waktuRealObj = moment(waktu_real, formatWaktuReal);

        const selisihJam = waktu1Obj.diff(waktuRealObj, "hours");
        // console.log(
        //   "🚀 ~ file: CreateGlucoseController.js:64 ~ checkTimeDifference ~ selisihJam:",
        //   selisihJam
        // );

        return selisihJam < 8 && selisihJam > -8;
      }

      let listError = [];
      if (!req.body?.code_users_child) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "code_users_child harus dikirim" },
          res
        );
      }

      if (
        req.body.checking_type != "Gula Darah 2 Jam Pasca Makan" &&
        req.body.checking_type != "Gula Darah Puasa" &&
        req.body.checking_type != "Gula Darah Sewaktu"
      ) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "jenis pemeriksaan tidak ditemukan" },
          res
        );
      }

      if (req.body.code_viva) {
        let vivacek = await findOne_master_viva({
          where: { code: req.body.code_viva, status: true },
        });
        if (!vivacek) {
          return error_handling(
            "Data Gagal Disimpan",
            403,
            { message: "Kode barang tidak tersedia" },
            res
          );
        }
      }

      // ambil users_login dari api/v1/customers/self/
      let find = await findOne_users_child({
        where: {
          code: req.body.code_users_child.toString(),
        },
      });
      if (!find) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "Kode pasien tidak ditemukan" },
          res
        );
      }
      let temp_id = decrypt("vivachek", req.body.code_users_child);
      let id = temp_id.replace("child ", "");
      let checkData = await findOne_users_child({
        where: { id: +id },
      });

      let createData;
      let result;
      if (checkData) {
        let checkTrial = await findOne_users({
          where: {
            id: checkData.users_id,
          },
        });
        if (checkTrial) {
          let pasien = await readAllusers_child({
            where: {
              users_id: checkTrial.id,
            },
          });
          let listIdPasien = [];
          pasien.forEach((element) => {
            listIdPasien.push(element.id);
          });
          let pasienGlucose = await readAllglucose_check({
            where: {
              users_child_id: { [Op.in]: listIdPasien },
              code_viva: { [Op.and]: [{ [Op.not]: null }, { [Op.not]: "" }] },
              // code_viva: { [Op.not]: null },
            },
          });
          if (
            checkTrial.isTrial == false &&
            !req.body.code_viva &&
            pasienGlucose.length == 0
          ) {
            return error_handling(
              "Data Gagal Disimpan",
              403,
              { message: "Akun trial sudah habis" },
              res
            );
          } else if (
            checkTrial.isTrial == false &&
            !req.body.code_viva &&
            pasienGlucose.length != 0
          ) {
            return error_handling(
              "Data Gagal Disimpan",
              403,
              { message: "Kode alat vivachek harus diisi" },
              res
            );
          }
        }

        //VALIDASI PEMERIKSAAN
        let validasi_tanggal = await findOne_glucose_check({
          where: {
            users_child_id: id,
            // checking_type: req.body.checking_type,
            inspection_time_date: new Date(req.body.inspection_time_date),
            inspection_time_hour: req.body.inspection_time_hour + ":00",
          },
        });
        if (validasi_tanggal) {
          return error_handling(
            "Data Gagal Disimpan",
            403,
            // `Tanggal ${req.body.inspection_time_date} dan waktu ${req.body.inspection_time_hour} sudah ada`,
            { message: `Maaf, kamu sudah cek gula darah di tanggal tersebut!` },
            res
          );
        } else {
          if (req.body.checking_type == "Gula Darah 2 Jam Pasca Makan") {
            let validasi_pemeriksaan = await findOne_glucose_check({
              where: {
                users_child_id: id,
                checking_type: { [Op.not]: "Gula Darah 2 Jam Pasca Makan" },
                inspection_time_date: new Date(req.body.inspection_time_date),
              },
            });
            if (validasi_pemeriksaan) {
              return error_handling(
                "Data Gagal Disimpan",
                403,
                {
                  message: `Maaf, dalam 1 hari hanya bisa memilih 1 jenis pemeriksaan yang sama`,
                },
                res
              );
            }
            let check_jam = await readAllglucose_check({
              where: {
                users_child_id: checkData.id,
                checking_type: "Gula Darah 2 Jam Pasca Makan",
                inspection_time_date: new Date(req.body.inspection_time_date),
              },
              order: [["inspection_time_hour", "ASC"]],
            });
            for (let i = 0; i < check_jam.length; i++) {
              const element = check_jam[i];
              let pengurangan = checkTimeDifference(
                element.inspection_time_hour,
                req.body.inspection_time_hour
              );
              if (pengurangan) {
                return error_handling(
                  "Data Gagal Disimpan",
                  403,
                  // `Waktu ${element.inspection_time_hour} belum 2 jam`,
                  {
                    message: `Maaf, kamu sudah cek gula darah(${element.inspection_time_hour}) jadi tunggu 2 jam setelah jam tersebut ya.`,
                  },
                  res
                );
              }
            }
          }
          if (req.body.checking_type == "Gula Darah Puasa") {
            let validasi_pemeriksaan = await findOne_glucose_check({
              where: {
                users_child_id: id,
                checking_type: { [Op.not]: "Gula Darah Puasa" },
                inspection_time_date: new Date(req.body.inspection_time_date),
              },
            });
            if (validasi_pemeriksaan) {
              return error_handling(
                "Data Gagal Disimpan",
                403,
                {
                  message: `Maaf, dalam 1 hari hanya bisa memilih 1 jenis pemeriksaan yang sama`,
                },
                res
              );
            }
            let validasi = await readAllglucose_check({
              where: {
                users_child_id: checkData.id,
                checking_type: "Gula Darah Puasa",
                inspection_time_date: new Date(req.body.inspection_time_date),
              },
            });
            // if (validasi.length == 1) {
            //   console.log('MELAKUKAN LEBIH DARI 2X PADA GULA DARAH PUASA');
            //   return error_handling(
            //     "Data Gagal Disimpan",
            //     403,
            //     `Maaf, kamu sudah cek gula darah di tanggal tersebut!`,
            //     // `Gula Darah Puasa sudah dilakukan 1x pada tanggal ${req.body.inspection_time_date}`,
            //     // `Maaf, kamu sudah cek gula darah(${element.inspection_time_hour}) jadi tunggu 8 jam setelah jam tersebut ya.`,
            //     res
            //   );
            // }
            for (let i = 0; i < validasi.length; i++) {
              const element = validasi[i];
              let pengurangan = checkTime8Difference(
                element.inspection_time_hour,
                req.body.inspection_time_hour
              );
              if (pengurangan) {
                return error_handling(
                  "Data Gagal Disimpan",
                  403,
                  // `Waktu ${element.inspection_time_hour} belum 2 jam`,
                  {
                    message: `Maaf, kamu sudah cek gula darah(${element.inspection_time_hour}) jadi tunggu 8 jam setelah jam tersebut ya.`,
                  },
                  res
                );
              }
            }
          }
          if (req.body.checking_type == "Gula Darah Sewaktu") {
            let validasi_pemeriksaan = await findOne_glucose_check({
              where: {
                users_child_id: id,
                checking_type: { [Op.not]: "Gula Darah Sewaktu" },
                inspection_time_date: new Date(req.body.inspection_time_date),
              },
            });
            if (validasi_pemeriksaan) {
              return error_handling(
                "Data Gagal Disimpan",
                403,
                {
                  message: `Maaf, dalam 1 hari hanya bisa memilih 1 jenis pemeriksaan yang sama`,
                },
                res
              );
            }
          }
        }

        let data = req.body.blood_sugar_value;
        let flag;
        let type = req.body.checking_type;
        if (type == "Gula Darah 2 Jam Pasca Makan") {
          if (data <= 70) {
            flag = "Rendah";
          } else if (data > 70 && data <= 139) {
            flag = "Normal";
          } else {
            flag = "Tinggi";
          }
        } else if (type == "Gula Darah Puasa") {
          if (data <= 70) {
            flag = "Rendah";
          } else if (data > 70 && data <= 125) {
            flag = "Normal";
          } else {
            flag = "Tinggi";
          }
        } else if (type == "Gula Darah Sewaktu") {
          if (data <= 70) {
            flag = "Rendah";
          } else if (data > 70 && data <= 199) {
            flag = "Normal";
          } else {
            flag = "Tinggi";
          }
        }
        createData = await createglucose_check({
          users_child_id: checkData.id,
          ...req.body,
          flag,
          created_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        });

        let child = await readAllusers_child({
          where: {
            users_id: checkData.users_id,
          },
        });
        let arrayChild = [];
        child.forEach((element) => {
          arrayChild.push(element.id);
        });
        let dataHead = await readAllglucose_check({
          where: {
            users_child_id: { [Op.in]: arrayChild },
          },
        });

        console.log(dataHead.length);
        if (dataHead.length == 10 || req.body.code_viva) {
          let updateData = await updateusers(
            { isTrial: false },
            { where: { id: checkData.users_id } }
          );
        }
        result = {
          code: createData.code,
          name: checkData.name,
          gender: checkData.gender,
          born: checkData.born,
          flag,
          blood_sugar_value: data,
          age: calculateAge(checkData.born),
        };
      } else {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "Users Child Id Tidak Ditemukan" },
          res
        );
      }

      if (req.body.code_viva) {
        let checkViva = await readAllglucose_check({
          where: {
            code_viva: req.body.code_viva,
          },
        });
        if (checkViva.length == 50) {
          let edit = await updatemaster_viva(
            { status: false },
            { where: { code: req.body.code_viva } }
          );
        }
      }
      return success("Data Berhasil Disimpan", 201, result, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling(
      "Data Gagal Disimpan",
      403,
      { message: error.message },
      res
    );
  }
}
