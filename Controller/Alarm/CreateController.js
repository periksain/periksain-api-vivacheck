import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createalarm,
  findOne_alarm,
  readAllalarm,
  updatealarm,
} from "../../Services/alarm/AlarmRepository.js";
import { Op } from "sequelize";
import { decrypt } from "../../Helper/Helper.js";
import { findOne_users_child } from "../../Services/users/UsersRepository.js";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Disimpan", 422, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      let listError = [];
      if (!req.body?.code_users_child) {
        listError.push({
          field: "code_users_child",
          message: `code_users_child harus dikirim`,
        });
        // return error_handling(
        //   "Data Gagal Disimpan",
        //   403,
        //   {message:"code_users_child harus dikirim"},
        //   res
        // );
      } if (!req.body.checking_type) {
        listError.push({
          field: "checking_type",
          message: `checking type harus dikirim`,
        });
      } if (!req.body.day) {
        listError.push({
          field: "day",
          message: `day harus dikirim`,
        });
      } if (!req.body.time) {
        listError.push({
          field: "time",
          message: `time harus dikirim`,
        });
      } if (!req.body?.repeat && req.body.repeat != false) {
        listError.push({
          field: "repeat",
          message: `repeat harus dikirim`,
        });
      } if (!req.body.ringtone) {
        listError.push({
          field: "ringtone",
          message: `ringtone harus dikirim`,
        });
      } if (!req.body?.shakes && req.body.shakes != false) {
        listError.push({
          field: "shakes",
          message: `shakes harus dikirim`,
        });
      } if (!req.body.note) {
        listError.push({
          field: "note",
          message: `note harus dikirim`,
        });
      }

      if (listError.length != 0) {
        return error_handling("Data Gagal Disimpan", 400, listError, res);
      }

      let temp_users_child = decrypt("vivachek", req.body.code_users_child);
      let users_child = temp_users_child.replace("child ", "");
      let cariChild = await findOne_users_child({
        where: {
          id: users_child,
        },
      });
      if (!cariChild) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          {message:"code_users_child tidak ditemukan"},
          res
        );
      }
      let check = await readAllalarm({
        where: {
          users_child_id: users_child,
        },
      });

      let idAlarm = 0;
      let listDay = [];
      if (check.length != 0) {
        let hari = req.body.day;
        let arrayHari = [];
        check.forEach((element) => {
          arrayHari.push({
            id: element.id,
            date: JSON.parse(element.date),
            waktu: element.time.substr(0, 5),
          });
        });
        for (let i = 0; i < hari.length; i++) {
          const element = hari[i];
          const indeksArray = arrayHari.find(
            (item) =>
              item.date.includes(element) &&
              item.waktu == req.body.time.substr(0, 5)
          );
          const indeksArrayTime = arrayHari.find(
            (item) => item.waktu == req.body.time.substr(0, 5)
          );
          if (indeksArray) {
            listError.push({
              field: "time and day",
              message: `Sudah ada untuk hari ${element} pukul ${req.body.time.substr(
                0,
                5
              )}`,
            });
          } else {
            if (indeksArrayTime) {
              idAlarm = indeksArrayTime.id;
              listDay = indeksArrayTime.date;
              req.body.day.forEach((it) => {
                listDay.push(it);
              });
            }
          }
        }
      }
      if (listError.length != 0) {
        return error_handling("Data Gagal Disimpan", 400, listError, res);
      }

      let createData;
      if (idAlarm != 0) {
        createData = await updatealarm(
          {
            ...req.body,
            date: listDay,
            updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          },
          { where: { id: idAlarm } }
        );
      } else {
        createData = await createalarm({
          ...req.body,
          date: req.body.day,
          users_child_id: users_child,
          created_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          status: true,
        });
      }

      return success("Data Berhasil Disimpan", 201, createData, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling(
      "Data Gagal Disimpan",
      403,
      { message: error.message },
      res
    );
  }
}
