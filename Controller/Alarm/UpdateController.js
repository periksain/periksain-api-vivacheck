import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createalarm,
  findOne_alarm,
  readAllalarm,
  updatealarm,
} from "../../Services/alarm/AlarmRepository.js";
import { Op } from "sequelize";
import { decrypt } from "../../Helper/Helper.js";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Disimpan", 422, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      let listError = [];
      if (!req.query?.code) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "code harus dikirim" },
          res
        );
      }
      if (!req.body.checking_type) {
        listError.push({
          field: "checking_type",
          message: `checking type harus dikirim`,
        });
      }
      if (!req.body.day) {
        listError.push({
          field: "day",
          message: `day harus dikirim`,
        });
      }
      if (!req.body.time) {
        listError.push({
          field: "time",
          message: `time harus dikirim`,
        });
      }
      if (!req.body?.repeat && req.body.repeat != false) {
        listError.push({
          field: "repeat",
          message: `repeat harus dikirim`,
        });
      }
      if (!req.body.ringtone) {
        listError.push({
          field: "ringtone",
          message: `ringtone harus dikirim`,
        });
      }
      if (!req.body?.shakes && req.body.shakes != false) {
        listError.push({
          field: "shakes",
          message: `shakes harus dikirim`,
        });
      }
      if (!req.body.note) {
        listError.push({
          field: "note",
          message: `note harus dikirim`,
        });
      }
      if (listError.length != 0) {
        return error_handling("Data Gagal Disimpan", 422, listError, res);
      }

      let temp_alarm = decrypt("vivachek", req.query.code);
      let alarm = temp_alarm.replace("alarm ", "");
      let getAlarm = await findOne_alarm({ where: { id: alarm } });
      if (!getAlarm) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "code alarm tidak ditemukan" },
          res
        );
      }
      let users_child = getAlarm.users_child_id;
      let check = await readAllalarm({
        where: {
          id: { [Op.not]: alarm },
          users_child_id: users_child,
        },
      });

      if (check.length != 0) {
        let hari = req.body.day;
        let arrayHari = [];
        check.forEach((element) => {
          arrayHari.push({
            id: element.id,
            date: JSON.parse(element.date),
            waktu: element.time.substr(0, 5),
          });
        });
        // console.log(
        //   "🚀 ~ file: UpdateController.js:67 ~ check.forEach ~ arrayHari:",
        //   arrayHari
        // );
        for (let i = 0; i < hari.length; i++) {
          const element = hari[i];
          const indeksArray = arrayHari.find(
            (item) =>
              item.date.includes(element) &&
              item.waktu == req.body.time.substr(0, 5)
          );
          if (indeksArray) {
            listError.push({
              field: "time and day",
              message: `Sudah ada untuk hari ${element} pukul ${req.body.time.substr(
                0,
                5
              )}`,
            });
          }
        }
      }
      if (listError.length != 0) {
        return error_handling("Data Gagal Disimpan", 422, listError, res);
      }

      let updateData = await updatealarm(
        {
          checking_type: req.body.checking_type,
          time: req.body.time,
          repeat: req.body.repeat,
          ringtone: req.body.ringtone,
          shakes: req.body.shakes,
          note: req.body.note,
          date: req.body.day,
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        },
        {
          where: {
            id: alarm,
          },
        }
      );

      return success("Data Berhasil Disimpan", 201, updateData, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling(
      "Data Gagal Disimpan",
      403,
      { message: error.message },
      res
    );
  }
}
