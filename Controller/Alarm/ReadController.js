import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import { readalarm } from "../../Services/alarm/AlarmRepository.js";

export default async function get(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling(
        "Read data gagal ditampilkan",
        422,
        errors.array(),
        res
      );
    } else {
      function getDateTime(date) {
        // var date = new Date();
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return (
          year + "-" + month + "-" + day
          // + " " + hour + ":" + min + ":" + sec
        );
      }
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      const users = req.query?.users_login || null;
      let tax = await readalarm(users);
      //   console.log(tax);
      //   let result = [];
      let temp = [];
      tax.users_children.forEach((element) => {
        if (element.alarms.length != 0) {
          //   result.push(element);
          element.alarms.forEach((item) => {
            temp.push({
              code: item.code,
              name: element.name,
              day: JSON.parse(item.date),
              time: item.time,
              note: item.note,
            });
          });
        }
      });

      //   let resultFix = Object.values(
      //     temp.reduce((acc, curr) => {
      //       const key = `${curr.name}-${curr.date}`;
      //       if (acc[key]) {
      //         acc[key].time.push(curr.time);
      //         acc[key].note.push(curr.note);
      //       } else {
      //         acc[key] = {
      //           name: curr.name,
      //           date: curr.date,
      //           time: [curr.time],
      //           note: [curr.note],
      //         };
      //       }
      //       return acc;
      //     }, {})
      //   );
      // temp.sort((a, b) => {
      //   const dateComparison = a.date.localeCompare(b.date);
      //   if (dateComparison !== 0) {
      //     return dateComparison;
      //   }
      //   return a.time.localeCompare(b.time);
      // });

      return success("Read data berhasil", 200, temp, res);
    }
  } catch (error) {
    console.error(error);
    return error_handling(
      "Read data gagal ditampilkan",
      403,
      {message:error.message},
      res
    );
  }
}
