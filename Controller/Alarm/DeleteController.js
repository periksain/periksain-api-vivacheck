import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createalarm,
  deletealarm,
  findOne_alarm,
  readAllalarm,
  updatealarm,
} from "../../Services/alarm/AlarmRepository.js";
import { Op } from "sequelize";
import { decrypt } from "../../Helper/Helper.js";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Dihapus", 422, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      let listError = [];
      if (!req.body?.code || req.body?.code.length == 0) {
        return error_handling(
          "Data Gagal Dihapus",
          403,
          { message: "code harus dikirim" },
          res
        );
      }

      let arrayAlarm = [];
      for (let i = 0; i < req.body.code.length; i++) {
        const element = req.body.code[i];
        let temp_alarm = decrypt("vivachek", element);
        let alarm = temp_alarm.replace("alarm ", "");
        arrayAlarm.push(alarm);
      }
      let getAlarm = await readAllalarm({
        where: { id: { [Op.in]: arrayAlarm } },
      });
      if (getAlarm == 0) {
        return error_handling(
          "Data Gagal Dihapus",
          403,
          { message: "code alarm tidak ditemukan" },
          res
        );
      } else {
        let hapus = await deletealarm({
          where: { id: { [Op.in]: arrayAlarm } },
          force: true,
        });
      }

      return success("Data Berhasil Dihapus", 201, getAlarm, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling(
      "Data Gagal Dihapus",
      403,
      { message: error.message },
      res
    );
  }
}
