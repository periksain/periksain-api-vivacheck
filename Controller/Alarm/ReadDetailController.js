import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createalarm,
  findOne_alarm,
  readAllalarm,
  updatealarm,
} from "../../Services/alarm/AlarmRepository.js";
import { Op } from "sequelize";
import { decrypt } from "../../Helper/Helper.js";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Disimpan", 422, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      let listError = [];
      if (!req.query?.code) {
        return error_handling(
          "Data Gagal Disimpan",
          400,
          [{ field: "code", message: "code harus dikirim" }],
          res
        );
      }

      let temp_alarm = decrypt("vivachek", req.query.code);
      let alarm = temp_alarm.replace("alarm ", "");
      let getAlarm = await findOne_alarm({ where: { id: alarm } });
      let result;
      if (!getAlarm) {
        return error_handling(
          "Data Gagal Disimpan",
          403,
          { message: "code alarm tidak ditemukan" },
          res
        );
      } else {
        result = {
          id: getAlarm.id,
          users_child_id: getAlarm.users_child_id,
          checking_type: getAlarm.checking_type,
          note: getAlarm.note,
          code: getAlarm.code,
          code_users_child: getAlarm.code_users_child,
          date: JSON.parse(getAlarm.date),
          time: getAlarm.time.substr(0, 5),
          ringtone: getAlarm.ringtone,
          repeat: getAlarm.repeat,
          shakes: getAlarm.shakes,
          status: getAlarm.status,
        };
      }

      return success("Data Berhasil Disimpan", 201, result, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling(
      "Data Gagal Disimpan",
      403,
      { message: error.message },
      res
    );
  }
}
