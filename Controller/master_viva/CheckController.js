import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  findOne_master_viva,
  readAllmaster_viva,
  readAllmaster_viva_check,
  updatemaster_viva,
} from "../../Services/master_viva/MasterVivaRepository.js";

export default async function get(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling(
        "Read data gagal ditampilkan",
        422,
        errors.array(),
        res
      );
    } else {
      let tax = await readAllmaster_viva_check();
      if (tax) {
        // let updateData = await updatemaster_viva(
        //   { status: false },
        //   { where: { id: tax.id } }
        // );
      } else {
        return error_handling(
          "Read data gagal ditampilkan",
          500,
          "Kode tidak sesuai",
          res
        );
      }

      return success("Read data berhasil", 200, tax, res);
    }
  } catch (error) {
    console.error(error);
    return error_handling(
      "Read data gagal ditampilkan",
      500,
      error.message,
      res
    );
  }
}
