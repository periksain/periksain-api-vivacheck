import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  findOne_master_viva,
  updatemaster_viva,
} from "../../Services/master_viva/MasterVivaRepository.js";
import { readAllglucose_check } from "../../Services/users/UsersRepository.js";

export default async function get(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling(
        "Read data gagal ditampilkan",
        422,
        errors.array(),
        res
      );
    } else {
      if (!req.body.code) {
        return error_handling(
          "Read data gagal ditampilkan",
          422,
          "code harus dikirim",
          res
        );
      }
      let tax = await findOne_master_viva({
        where: {
          code: req.body.code,
          status: true,
        },
      });
      if (tax) {
        // let updateData = await updatemaster_viva(
        //   { status: false },
        //   { where: { id: tax.id } }
        // );
      } else {
        return error_handling(
          "Read data gagal ditampilkan",
          500,
          "Kode tidak sesuai",
          res
        );
      }

      let checkViva = await readAllglucose_check({
        where: {
          code_viva: req.body.code,
        },
      });

      tax.dataValues.tersisa = 50 - +(checkViva?.length)
      return success("Read data berhasil", 200, tax, res);
    }
  } catch (error) {
    console.error(error);
    return error_handling(
      "Read data gagal ditampilkan",
      500,
      error.message,
      res
    );
  }
}
