import success from "../../Helper/Response/success.js";
import error_handling from "../../Helper/Response/error.js";
import { validationResult } from "express-validator";
import {
  createmaster_viva,
  createmaster_vivabulk,
  findOne_master_viva,
  getLastMaster_viva,
  readAllmaster_viva,
} from "../../Services/master_viva/MasterVivaRepository.js";
import { Op } from "sequelize";

export default async function createOne(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return error_handling("Data Gagal Disimpan", 422, errors.array(), res);
    } else {
      console.log("req ", req.body);
      function convertTZ(date, tzString) {
        return new Date(
          (typeof date === "string" ? new Date(date) : date).toLocaleString(
            "en-US",
            { timeZone: tzString }
          )
        );
      }

      function getDate(date) {
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        year = year.toString();
        return year.substr(year.length - 2) + "" + month + "" + day;
      }
      if (!req.body.looping) {
        return error_handling("Data Gagal Disimpan", 400, [{field:"looping",message:"looping harus dikirim"}], res);
      }
      let looping = req.body.looping;

      function generateRandomCode() {
        const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const numbers = "0123456789";

        let randomCode = "";

        // Generate 3 random letters
        for (let i = 0; i < 5; i++) {
          const randomIndex = Math.floor(Math.random() * characters.length);
          randomCode += characters.charAt(randomIndex);
        }

        // Generate 3 random numbers
        for (let i = 0; i < 5; i++) {
          const randomIndex = Math.floor(Math.random() * numbers.length);
          randomCode += numbers.charAt(randomIndex);
        }

        return randomCode;
      }

      let array = [];
      for (let i = 0; i < looping; i++) {
        let isDuplicate = false;
        let randomCode;

        do {
          randomCode = generateRandomCode();
          isDuplicate = array.some((item) => item.code === randomCode);
        } while (isDuplicate);

        array.push({
          code: randomCode,
          status: true,
          created_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
          updated_at: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        });
      }

      var createData = await createmaster_vivabulk(array);

      return success("Data Berhasil Disimpan", 201, createData, res);
    }
  } catch (error) {
    console.log(error);
    return error_handling("Data Gagal Disimpan", 500, error.message, res);
  }
}
