import { usersRoutes } from "../Services/users/UsersRoutes.js";
import { vivaRoutes } from "../Services/master_viva/MasterVivaRoutes.js";
import { alarmRoutes } from "../Services/alarm/AlarmRoutes.js";


const MainRoutes = (app) => {
  usersRoutes(app)
  vivaRoutes(app)
  alarmRoutes(app)
};

export default MainRoutes;
