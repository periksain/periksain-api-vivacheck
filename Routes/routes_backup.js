import express from "express";
const router = express.Router();

import create_m_barang from "../Controller/m_barang/CreateController.js";
import read_m_barang from "../Controller/m_barang/ReadController.js";
import update_m_barang from "../Controller/m_barang/UpdateController.js";
import validator_m_barang from "../Validator/BarangValidator.js"

router.post(
    "/api/v1/m_barang/create",
    validator_m_barang("create"), 
    create_m_barang
    );
router.get("/api/v1/m_barang/read",validator_m_barang("read"), read_m_barang);
router.put(
    "/api/v1/m_barang/update",
    validator_m_barang("update"),
    update_m_barang
  );

import create_m_jenis_barang from "../Controller/m_jenis_barang/CreateController.js";
import read_m_jenis_barang from "../Controller/m_jenis_barang/ReadController.js";
import update_m_jenis_barang from "../Controller/m_jenis_barang/UpdateController.js";
import validator_m_jenis_barang from "../Validator/JenisBarangValidator.js"

router.post(
    "/api/v1/m_jenis_barang/create",
    validator_m_jenis_barang("create"), 
    create_m_jenis_barang
    );
router.get("/api/v1/m_jenis_barang/read",validator_m_jenis_barang("read"), read_m_jenis_barang);
router.put(
    "/api/v1/m_jenis_barang/update",
    validator_m_jenis_barang("update"),
    update_m_jenis_barang
  );

import create_m_kategori_barang from "../Controller/m_kategori_barang/CreateController.js";
import read_m_kategori_barang from "../Controller/m_kategori_barang/ReadController.js";
import update_m_kategori_barang from "../Controller/m_kategori_barang/UpdateController.js";
import validator_m_kategori_barang from "../Validator/KategoriBarangValidator.js"

router.post(
    "/api/v1/m_kategori_barang/create",
    validator_m_kategori_barang("create"), 
    create_m_kategori_barang
    );
router.get("/api/v1/m_kategori_barang/read",validator_m_kategori_barang("read"), read_m_kategori_barang);
router.put(
    "/api/v1/m_kategori_barang/update",
    validator_m_kategori_barang("update"),
    update_m_kategori_barang
  );

import create_m_alat from "../Controller/m_alat/CreateController.js";
import read_m_alat from "../Controller/m_kategori_barang/ReadController.js";
import update_m_alat from "../Controller/m_kategori_barang/UpdateController.js";
import validator_m_alat from "../Validator/KategoriBarangValidator.js"

router.post(
    "/api/v1/m_alat/create",
    validator_m_alat("create"), 
    create_m_alat
    );
router.get("/api/v1/m_alat/read",validator_m_alat("read"), read_m_alat);
router.put(
    "/api/v1/m_alat/update",
    validator_m_alat("update"),
    update_m_alat
  );

import create_m_konversi_barang from "../Controller/m_konversi_barang/CreateController.js";
import read_m_konversi_barang from "../Controller/m_konversi_barang/ReadController.js";
import update_m_konversi_barang from "../Controller/m_konversi_barang/UpdateController.js";
import validator_m_konversi_barang from "../Validator/KonversiBarangValidator.js"

router.post(
    "/api/v1/m_konversi_barang/create",
    validator_m_konversi_barang("create"), 
    create_m_konversi_barang
    );
router.get("/api/v1/m_konversi_barang/read",validator_m_konversi_barang("read"), read_m_konversi_barang);
router.put(
    "/api/v1/m_konversi_barang/update",
    validator_m_konversi_barang("update"),
    update_m_konversi_barang
  );

import create_m_satuan from "../Controller/m_satuan/CreateController.js";
import read_m_satuan from "../Controller/m_satuan/ReadController.js";
import update_m_satuan from "../Controller/m_satuan/UpdateController.js";
import validator_m_satuan from "../Validator/SatuanBarangValidator.js"

router.post(
    "/api/v1/m_satuan/create",
    validator_m_satuan("create"), 
    create_m_satuan
    );
router.get("/api/v1/m_satuan/read",validator_m_satuan("read"), read_m_satuan);
router.put(
    "/api/v1/m_satuan_barang/update",
    validator_m_satuan("update"),
    update_m_satuan
  );

import create_m_supplier from "../Controller/m_supplier/CreateController.js";
import read_m_supplier from "../Controller/m_supplier/ReadController.js";
import update_m_supplier from "../Controller/m_supplier/UpdateController.js";
import validator_m_supplier from "../Validator/SupplierValidator.js"

router.post(
    "/api/v1/m_supplier/create",
    validator_m_supplier("create"), 
    create_m_supplier
    );
router.get("/api/v1/m_supplier/read",validator_m_supplier("read"), read_m_supplier);
router.put(
    "/api/v1/m_supplier/update",
    validator_m_supplier("update"),
    update_m_supplier
  );

import create_m_jenis_konsumsi from "../Controller/m_jenis_konsumsi/CreateController.js";
import read_m_jenis_konsumsi from "../Controller/m_jenis_konsumsi/ReadController.js";
import update_m_jenis_konsumsi from "../Controller/m_jenis_konsumsi/UpdateController.js";
import validator_m_jenis_konsumsi from "../Validator/JenisKonsumsiValidator.js"

router.post(
    "/api/v1/jenis_konsumsi/create",
    validator_m_jenis_konsumsi("create"), 
    create_m_jenis_konsumsi
    );
router.get("/api/v1/jenis_konsumsi/read",validator_m_jenis_konsumsi("read"), read_m_jenis_konsumsi);
router.put(
    "/api/v1/jenis_konsumsi/update",
    validator_m_jenis_konsumsi("update"),
    update_m_jenis_konsumsi
  );

import create_m_konsumsi from "../Controller/m_konsumsi/CreateController.js";
import read_m_konsumsi from "../Controller/m_jenis_konsumsi/ReadController.js";
import update_m_konsumsi from "../Controller/m_konsumsi/UpdateController.js";
import validator_m_konsumsi from "../Validator/KonsumsiValidator.js"

router.post(
    "/api/v1/konsumsi/create",
    validator_m_konsumsi("create"), 
    create_m_konsumsi
    );
router.get("/api/v1/konsumsi/read",validator_m_konsumsi("read"), read_m_konsumsi);
router.put(
    "/api/v1/konsumsi/update",
    validator_m_konsumsi("update"),
    update_m_konsumsi
  );

import create_t_barang_masuk from "../Controller/t_barang_masuk/CreateController.js";
import read_t_barang_masuk from "../Controller/t_barang_masuk/ReadController.js";
import update_t_barang_masuk from "../Controller/t_barang_masuk/UpdateController.js";
import validator_t_barang_masuk from "../Validator/TransaksiBarangMasukValidator.js"

router.post(
    "/api/v1/t_barang_masuk/create",
    validator_t_barang_masuk("create"), 
    create_t_barang_masuk
    );
router.get("/api/v1/t_barang_masuk/read",validator_t_barang_masuk("read"), read_t_barang_masuk);
router.put(
    "/api/v1/t_barang_masuk/update",
    validator_t_barang_masuk("update"),
    update_t_barang_masuk
  );

import create_stock_barang from "../Controller/m_stock_barang/CreateController.js";
import read_stock_barang from "../Controller/m_stock_barang/ReadController.js";
import update_stock_barang from "../Controller/m_stock_barang/UpdateController.js";
import validator_stock_barang from "../Validator/StockBarangValidator.js"

router.post(
    "/api/v1/stock_barang/create",
    validator_stock_barang("create"), 
    create_stock_barang
    );
router.get("/api/v1/stock_barang/read",validator_stock_barang("read"), read_stock_barang);
router.put(
    "/api/v1/stock_barang/update",
    validator_stock_barang("update"),
    update_stock_barang
  );

import create_m_jenis_barang_keluar from "../Controller/m_jenis_barang_keluar/CreateController.js";
import read_m_jenis_barang_keluar from "../Controller/m_jenis_barang_keluar/ReadController.js";
import update_m_jenis_barang_keluar from "../Controller/m_jenis_barang_keluar/UpdateController.js";
import validator_m_jenis_barang_keluar from "../Validator/JenisBarangKeluarValidator.js"

router.post(
    "/api/v1/jenis_barang_keluar/create",
    validator_m_jenis_barang_keluar("create"), 
    create_m_jenis_barang_keluar
    );
router.get("/api/v1/jenis_barang_keluar/read",validator_m_jenis_barang_keluar("read"), read_m_jenis_barang_keluar);
router.put(
    "/api/v1/jenis_barang_keluar/update",
    validator_m_jenis_barang_keluar("update"),
    update_m_jenis_barang_keluar
  );

import create_m_barang_keluar from "../Controller/m_barang_keluar/CreateController.js";
import read_m_barang_keluar from "../Controller/m_barang_keluar/ReadController.js";
import update_m_barang_keluar from "../Controller/m_barang_keluar/UpdateController.js";
import validator_m_barang_keluar from "../Validator/BarangKeluarValidator.js"

router.post(
    "/api/v1/barang_keluar/create",
    validator_m_barang_keluar("create"), 
    create_m_barang_keluar
    );
router.get("/api/v1/barang_keluar/read",validator_m_barang_keluar("read"), read_m_barang_keluar);
router.put(
    "/api/v1/barang_keluar/update",
    validator_m_barang_keluar("update"),
    update_m_barang_keluar
  );