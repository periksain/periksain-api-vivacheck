import DataTypes from "sequelize";
import db from "../Config/config.js";

const attributes = {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: null,
    comment: null,
    primaryKey: true,
    field: "id",
    autoIncrement: true,
  },
  name: {
    type: DataTypes.CHAR(250),
    allowNull: true,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "name",
    autoIncrement: false,
  },
  isTrial: {
    type: DataTypes.BOOLEAN,
    allowNull: true,
    defaultValue: true,
    comment: null,
    primaryKey: false,
    field: "isTrial",
    autoIncrement: false,
  },
  created_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "created_at",
    autoIncrement: false,
  },
  updated_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "updated_at",
    autoIncrement: false,
  },
};
const options = {
  freezeTableName: true,
  timestamps: false,
  tableName: "users",
  comment: "",
  indexes: [],
};
const users = db.define("users", attributes, options);
export default users;
