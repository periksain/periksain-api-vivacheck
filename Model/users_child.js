import DataTypes from "sequelize";
import db from "../Config/config.js";

const attributes = {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: null,
    comment: null,
    primaryKey: true,
    field: "id",
    autoIncrement: true,
  },
  users_id: {
    type: DataTypes.INTEGER,
    allowNull: true,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "users_id",
    autoIncrement: false,
  },
  name: {
    type: DataTypes.CHAR(250),
    allowNull: true,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "name",
    autoIncrement: false,
  },
  code: {
    type: DataTypes.CHAR(250),
    allowNull: true,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "code",
    autoIncrement: false,
  },
  born: {
    type: DataTypes.DATE,
    allowNull: true,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "born",
    autoIncrement: false,
  },
  gender: {
    type: DataTypes.CHAR(250),
    allowNull: true,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "gender",
    autoIncrement: false,
  },
  created_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "created_at",
    autoIncrement: false,
  },
  updated_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: null,
    comment: null,
    primaryKey: false,
    field: "updated_at",
    autoIncrement: false,
  },
};
const options = {
  freezeTableName: true,
  timestamps: false,
  tableName: "users_child",
  comment: "",
  indexes: [],
};
const users_child = db.define("users_child", attributes, options);
export default users_child;
