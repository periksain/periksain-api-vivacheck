--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alarm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alarm (
    id integer NOT NULL,
    users_child_id bigint,
    checking_type character varying,
    date character varying,
    "time" time without time zone,
    repeat boolean,
    ringtone text,
    shakes boolean,
    status boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    note character varying,
    code character varying,
    code_users_child character varying
);


ALTER TABLE public.alarm OWNER TO postgres;

--
-- Name: alarm_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.alarm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alarm_id_seq OWNER TO postgres;

--
-- Name: alarm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.alarm_id_seq OWNED BY public.alarm.id;


--
-- Name: glucose_check; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.glucose_check (
    id integer NOT NULL,
    users_child_id bigint,
    checking_type character varying,
    inspection_time_date date,
    blood_sugar_value double precision,
    flag character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    inspection_time_hour time without time zone,
    code_viva character varying,
    code_users_child character varying,
    code character varying,
    status boolean
);


ALTER TABLE public.glucose_check OWNER TO postgres;

--
-- Name: glucose_check_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.glucose_check_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.glucose_check_id_seq OWNER TO postgres;

--
-- Name: glucose_check_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.glucose_check_id_seq OWNED BY public.glucose_check.id;


--
-- Name: master_viva; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.master_viva (
    id integer NOT NULL,
    code character varying,
    status boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.master_viva OWNER TO postgres;

--
-- Name: master_viva_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.master_viva_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.master_viva_id_seq OWNER TO postgres;

--
-- Name: master_viva_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.master_viva_id_seq OWNED BY public.master_viva.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    "isTrial" boolean
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_child; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_child (
    id integer NOT NULL,
    users_id bigint,
    name character varying,
    born date,
    gender character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    code character varying
);


ALTER TABLE public.users_child OWNER TO postgres;

--
-- Name: users_child_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_child_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_child_id_seq OWNER TO postgres;

--
-- Name: users_child_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_child_id_seq OWNED BY public.users_child.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: alarm id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alarm ALTER COLUMN id SET DEFAULT nextval('public.alarm_id_seq'::regclass);


--
-- Name: glucose_check id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.glucose_check ALTER COLUMN id SET DEFAULT nextval('public.glucose_check_id_seq'::regclass);


--
-- Name: master_viva id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_viva ALTER COLUMN id SET DEFAULT nextval('public.master_viva_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: users_child id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_child ALTER COLUMN id SET DEFAULT nextval('public.users_child_id_seq'::regclass);


--
-- Name: alarm alarm_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alarm
    ADD CONSTRAINT alarm_pkey PRIMARY KEY (id);


--
-- Name: glucose_check glucose_check_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.glucose_check
    ADD CONSTRAINT glucose_check_pkey PRIMARY KEY (id);


--
-- Name: master_viva master_viva_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_viva
    ADD CONSTRAINT master_viva_pkey PRIMARY KEY (id);


--
-- Name: users_child users_child_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_child
    ADD CONSTRAINT users_child_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

