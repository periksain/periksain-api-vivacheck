# Vivachek

## Deskripsi
Vivachek adalah fitur tambahan dari aplikasi periksain yang memungkinkan pengguna untuk melakukan pemeriksaan kesehatan secara mandiri. Aplikasi ini menyediakan pengukuran gula darah.

## Cara Menggunakan
1. Clone repository ini dengan perintah `git clone https://github.com/adamlabs-be/vivachek.git`.
2. Install dependensi dengan perintah `npm install`.
3. Buat file `.env` dengan menyalin isi dari file `.env_example`.
4. Jalankan aplikasi dengan perintah `npm run dev`.

## Lisensi
Proyek ini dilisensikan di bawah lisensi MIT.
