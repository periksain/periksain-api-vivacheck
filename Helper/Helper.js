import moment from "moment";

// export default async function convert_time(time) {
//   if (time === null || time === undefined) {
//     // console.log('masuk null');
//     return null;
//   }
//   return momentt(time).format("YYYY,MM,DD");
// }

async function convertTZ(date, tzString) {
  return new Date(
    (typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {
      timeZone: tzString,
    })
  );
}

const convert_time = (time) => {
  if (time === null || time === undefined) {
    // console.log('masuk null');
    return null;
  }
  console.log(time);
  let result = moment(time).local().format("HH:mm:ss");
  console.log(result);
  return result;
};

async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}

const SALT = "vivachek";

function crypt(salt, text) {
  const textToChars = (text) => text.split("").map((c) => c.charCodeAt(0));
  const byteHex = (n) => ("0" + Number(n).toString(16)).substr(-2);
  const applySaltToChar = (code) =>
    textToChars(salt).reduce((a, b) => a ^ b, code);

  return text
    .split("")
    .map(textToChars)
    .map(applySaltToChar)
    .map(byteHex)
    .join("");
}

function decrypt(salt, encoded) {
  const textToChars = (text) => text.split("").map((c) => c.charCodeAt(0));
  const applySaltToChar = (code) =>
    textToChars(salt).reduce((a, b) => a ^ b, code);
  return encoded
    .match(/.{1,2}/g)
    .map((hex) => parseInt(hex, 16))
    .map(applySaltToChar)
    .map((charCode) => String.fromCharCode(charCode))
    .join("");
}

export { convert_time, loop, convertTZ, crypt, decrypt };
