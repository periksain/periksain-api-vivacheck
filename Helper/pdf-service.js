import axios from "axios";
import pdprinter from "pdfmake";
import jsdom from "jsdom";
const { JSDOM } = jsdom;
const { window } = new JSDOM("");
import htmlToPdfmake from "html-to-pdfmake";
import { footer, header } from "./image.js";
const PdfPrinter = pdprinter;

async function buildPDF(olahan, jenis_pemeriksaan, olahan_atas) {
  return new Promise(async (resolve, reject) => {
    // await $getBase64ImageFromURL(
    //   app.store.state.pengaturanUmum && app.store.state.pengaturanUmum.logo
    //     ? app.store.state.pengaturanUmum.logo
    //     : '/logo-header.png'
    // '/logo-header.png'
    // )

    var docDefinition = {
      header: function (currentPage, pageCount, pageSize) {
        return [
          {
            margin: [0, 0, 30, 0],
            columns: [
              // {
              //   image: convertHeader,
              //   fit: [100, 40],
              //   margin: [0, 10, 0, 0],
              //   width: 100,
              // },
              // {
              //   stack: [
              //     { text: "PT. Kreasi Periksa Indonesia", bold: true, fontSize: "11" },
              //     {
              //       text: "Perum Bogorami Indah Regency\nJl. Bulak Setro Indah 2 Blok B No. 21, Surabaya",
              //       fontSize: "9",
              //       // font: 'Arial'
              //     },
              //   ],
              //   margin: [0, 0, 0, 0],
              //   width: "*",
              // },
              // {
              //   margin: [5, 25, 10, 0],
              //   text: "VIVACHEK",
              //   fontSize: "10",
              //   bold: true,
              //   // stack: [
              //   //   { text: 'E: ' + dataPegawai.email, fontSize: '8' },
              //   // ],
              //   alignment: "right",
              //   width: 120,
              // },
              {
                image: header,
                width: 600,
              },
            ],
          },
          // {
          //   margin: [30, 5, 30, 0],
          //   canvas: [
          //     {
          //       type: "line",
          //       x1: 0,
          //       y1: 1,
          //       x2: 535,
          //       y2: 1,
          //       lineWidth: 1,
          //       lineColor: "black",
          //     },
          //   ],
          // },
        ];
      },
      // footer: function (currentPage, pageCount, pageSize) {
      //   return {
      //     alignment: 'center',
      //     image: footerImage,
      //     height: 95,
      //     width: pageSize.width,
      //   }
      // },
      // background: function (currentPage, pageSize) {
      //   return {
      //     alignment: 'center',
      //     margin: [0, (pageSize.height * 3) / 5, 0, 0],
      //     image: background,
      //     opacity: 0.3,
      //     width: pageSize.width,
      //   }
      // },

      footer: [
        {
          margin: [0, 15, 0, 0],
          columns: [
            {
              image: footer,
              width: 600,
            },
          ],
        },
      ],
      pageMargins: [30, 100, 30, 100],

      content: [
        {
          margin: [30, 0, 30, 10],
          columns: [
            {
              fontSize: 10,
              // margin: [30, 0, 30, 0],
              table: {
                headerRows: 1,
                widths: [85, 2, "*"],
                body: [
                  [
                    {
                      text: [
                        { text: "Nama Pasien", bold: true },
                        // { text: 'Patient Name', italics: true, fontSize: '7' },
                      ],
                    },
                    { text: ":" },
                    { text: olahan_atas.nama },
                  ],
                  [
                    {
                      text: [
                        { text: "Tanggal Lahir", bold: true },
                        // { text: 'Date of Birth / Age', italics: true, fontSize: '7' },
                      ],
                    },
                    { text: ":" },
                    {
                      text: olahan_atas.tgl_lahir,
                    },
                  ],
                  [
                    {
                      text: [
                        { text: "Umur", bold: true },
                        // { text: 'Referring Doctor', italics: true, fontSize: '7' },
                      ],
                    },
                    { text: ":" },
                    { text: olahan_atas.umur },
                  ],
                  // [
                  //   {
                  //     text: [
                  //       { text: 'Jenis Kelamin', bold: true },
                  //       // { text: 'Gender', italics: true, fontSize: '7' },
                  //     ],
                  //   },
                  //   { text: ':' },
                  //   { text: dataTable.pasien.jenis_kelamin },
                  // ],
                  // [
                  //   {
                  //     text: [
                  //       { text: "Jenis Kelamin", bold: true },
                  //       // { text: 'Diagnosis', italics: true, fontSize: '7' },
                  //     ],
                  //   },
                  //   { text: ":" },
                  //   { text: "isi diagnosa" },
                  // ],
                  // [
                  //   {
                  //     text: [
                  //       { text: "Alamat", bold: true },
                  //       // { text: 'Address', italics: true, fontSize: '7' },
                  //     ],
                  //   },
                  //   { text: ":" },
                  //   {
                  //     text: "isi alamat",
                  //     // dataTable.pasien.alamat +
                  //     // (dataTable.pasien.nama_kecamatan
                  //     //   ? ', ' + dataTable.pasien.nama_kecamatan
                  //     //   : '') +
                  //     // (dataTable.pasien.nama_kabupaten
                  //     //   ? ', ' + dataTable.pasien.nama_kabupaten
                  //     //   : '') +
                  //     // (dataTable.pasien.nama_provinsi
                  //     //   ? ', ' + dataTable.pasien.nama_provinsi
                  //     //   : ''),
                  //   },
                  // ],
                ],
              },
              layout: "noBorders",
              width: "60%",
            },
            {
              fontSize: 10,
              table: {
                headerRows: 1,
                widths: [85, 2, "*"],
                body: [
                  // [
                  //   {
                  //     text: [
                  //       { text: 'No. Laboratorium', bold: true },
                  //       // { text: 'Laboratory Number', italics: true, fontSize: '7' },
                  //     ],
                  //   },
                  //   { text: ':' },
                  //   { text: dataTable.registrasi.no_lab },
                  // ],
                  [
                    {
                      text: [
                        { text: "Jenis Kelamin", bold: true },
                        // { text: 'Date Registration', italics: true, fontSize: '7' },
                      ],
                    },
                    { text: ":" },
                    {
                      text: olahan_atas.jenis_kelamin,
                    },
                  ],
                  [
                    {
                      text: [
                        { text: "Tanggal Periksa", bold: true },
                        // { text: 'Date Reported', italics: true, fontSize: '7' },
                      ],
                    },
                    { text: ":" },
                    {
                      text: olahan_atas.tanggal_periksa,
                    },
                  ],
                  [
                    {
                      text: [
                        { text: "Waktu Periksa", bold: true },
                        // { text: 'MR Number', italics: true, fontSize: '7' },
                      ],
                    },
                    { text: ":" },
                    { text: olahan_atas.waktu_periksa },
                  ],
                  // [
                  //   {
                  //     text: [{ text: "Ruangan / Kelas", bold: true }],
                  //   },
                  //   { text: ":" },
                  //   {
                  //     text: "isi ruangan",
                  //   },
                  // ],
                  // [
                  //   {
                  //     text: [{ text: "Penjamin", bold: true }],
                  //   },
                  //   { text: ":" },
                  //   {
                  //     text: `isi penjamin`,
                  //   },
                  // ],
                ],
              },
              layout: "noBorders",
              width: "40%",
            },
          ],
        },

        // {
        //   text: "Gula Darah",
        //   style: "subHeader",
        //   alignment: "center",
        //   margin: [0, 0, 0, 10],
        // },

        {
          style: "tableExample",
          color: "#444",
          alignment: "center",
          width: "100%",
          table: {
            widths: ["*", "*", "*", "*"],
            // headerRows: 2,
            // keepWithHeaderRows: 1,
            body: [
              [
                {
                  text: "Hasil Pemeriksaan " + jenis_pemeriksaan,
                  style: "tableHeader",
                  colSpan: 4,
                  alignment: "center",
                  fontSize: 12,
                  margin: [0, 7, 0, 7],
                },
                {},
                {},
                {},
              ],
              [
                {
                  text: "Tanggal",
                  style: "tableHeader",
                  alignment: "center",
                  fontSize: 11,
                  margin: [0, 7, 0, 7],
                },
                {
                  text: "Nilai Rujukan",
                  style: "tableHeader",
                  alignment: "center",
                  fontSize: 11,
                  margin: [0, 7, 0, 7],
                },
                {
                  text: "Hasil Pemeriksaan",
                  style: "tableHeader",
                  alignment: "center",
                  fontSize: 11,
                  margin: [0, 7, 0, 7],
                },
                {
                  text: "Status",
                  style: "tableHeader",
                  alignment: "center",
                  fontSize: 11,
                  margin: [0, 7, 0, 7],
                },
              ],
              [
                {
                  text: olahan[0],
                  fontSize: 10,
                  margin: [0, 7, 0, 7],
                },
                {
                  text: olahan[1],
                  fontSize: 8,
                  margin: [0, 7, 0, 7],
                },
                {
                  text: olahan[2]+" mg/dL",
                  fontSize: 10,
                  margin: [0, 7, 0, 7],
                },
                {
                  text: olahan[3],
                  fontSize: 10,
                  margin: [0, 7, 0, 7],
                },
              ],
            ],
          },
        },
      ],

      // pageBreakBefore(
      //   currentNode,
      //   followingNodesOnPage,
      //   nodesOnNextPage,
      //   previousNodesOnPage
      // ) {
      //   if (currentNode.pageNumbers.length > 1 && currentNode.unbreakable) {
      //     return true;
      //   }

      //   if (
      //     currentNode.headlineLevel == 5 &&
      //     nodesOnNextPage[0].pageNumbers.length > 1
      //   ) {
      //     currentNode.pageBreak = "after";
      //     return false;
      //   }

      //   return false;
      // },

      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 2],
        },
        subHeader: {
          fontSize: 10,
          bold: true,
        },
        tableExample: {
          margin: [30, 0, 25, 15],
        },
        tableHeader: {
          bold: true,
          fontSize: 9,
          color: "black",
        },
        detailInfo: {
          fontSize: 11,
          margin: [0, 0, 0, 0],
        },
      },
      defaultStyle: {
        color: "black",
        columnGap: 20,
        // font: 'Arial',
      },
    };

    // playground requires you to assign document definition to a variable called dd

    let fonts = {
      // Roboto: {
      //   normal: "Helper/roboto-font/Roboto/roboto-regular-webfont.ttf",
      //   bold: "Helper/roboto-font/Roboto/roboto-bold-webfont.ttf",
      //   italics: "Helper/roboto-font/Roboto/roboto-italic-webfont.ttf",
      //   bolditalics: "Helper/roboto-font/Roboto/roboto-bolditalic-webfont.ttf",
      // },
      Roboto: {
        normal: "Helper/Font_Inter/Inter/Inter-Regular.ttf",
        bold: "Helper/Font_Inter/Inter/Inter-Bold.ttf",
        italics: "Helper/Font_Inter/Inter/Inter-Regular.ttf",
        bolditalics: "Helper/Font_Inter/Inter/Inter-Bold.ttf",
      },
    };

    var printer = new PdfPrinter(fonts);
    // var fs = require('fs');

    var options = {
      // ...
    };
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options);
    // pdfDoc.pipe(fs.createWriteStream("./public/pdf/hasil/" + fileName + ".pdf"));
    var chunks = [];
    pdfDoc.on("data", function (chunk) {
      chunks.push(chunk);
    });
    var result64;
    pdfDoc.on("end", async function () {
      var result = Buffer.concat(chunks);
      // console.log('result ', result);
      result64 = Buffer.from(result).toString("base64");
      // console.log("result64 ", result64);
      // await updatepembayaran({ pdf: result64 }, { where: { id: pembayaran.id } });
      console.log("result64 ", true);
      resolve(result64);
    });
    pdfDoc.end();
  });
}

export default {
  buildPDF,
};
