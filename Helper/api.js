import dotenv from "dotenv";
dotenv.config();

const api_datamaster = process.env.API_DATAMASTER;
const api_registrasi = process.env.API_REGISTRASI;
const api_inputhasil = process.env.API_INPUTHASIL;
const api_arsip = process.env.API_ARSIP;
const api_qc = process.env.API_QC;
const api_gateway = process.env.API_GATEWAY;
const api_rekammedis = process.env.API_REKAMMEDIS;
const api_rawatjalan = process.env.API_RAWATJALAN;
const api_rawatinap = process.env.API_RAWATINAP;
const api_pembayaran = process.env.API_PEMBAYARAN;
const api_lab = process.env.API_LAB;
const api_ws = process.env.API_WEBSOCKET;

export {
  api_datamaster,
  api_registrasi,
  api_inputhasil,
  api_arsip,
  api_gateway,
  api_qc,
  api_rekammedis,
  api_rawatjalan,
  api_rawatinap,
  api_pembayaran,
  api_lab,
  api_ws
};
