export default function error(message, code, description, res) {
  // var res = this.res;
  var data = {
    success: false,
    message: message,
    code: code,
    errors: description,
    meta: null
  };
  console.table({
    success: false,
    message: message,
    code: code,
    errors: description,
  });
  res.status(code)
  return res.json(data);
}
