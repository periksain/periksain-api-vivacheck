export default function success(message, code, data, res, total, page, page_size, tambahan) {
    console.log(message);
    if (
      total === undefined &&
      page === undefined &&
      page_size === undefined &&
      data === undefined
    ) {
      var data = {
        success: true,
        code: code,
        message: message,
      };
      return res.json(data);
    }
  
    if (
      total === undefined &&
      page === undefined &&
      page_size === undefined &&
      data !== undefined
    ) {
      var data = {
        success: true,
        code: code,
        message: message,
        data: data,
        meta: {}
        // payload: data,
      };
      return res.json(data);
    }
  
    if (
      total !== undefined &&
      page !== undefined &&
      page_size !== undefined &&
      data !== undefined &&
      tambahan === undefined
    ) {
      var data = {
        success: true,
        code: code,
        message: message,
        properties: {
          page: page,
          total: total,
          page_size: page_size,
        },
        data: data,
        // payload: data,
      };
      return res.json(data);
    }
    
    if (
      total !== undefined &&
      page !== undefined &&
      page_size !== undefined &&
      data !== undefined &&
      tambahan !== undefined
    ) {
      var data = {
        success: true,
        code: code,
        message: message,
        properties: {
          page: page,
          total: total,
          page_size: page_size,
          expired: tambahan
        },
        data: data,
        // payload: data,
      };
      return res.json(data);
    }
  }
  