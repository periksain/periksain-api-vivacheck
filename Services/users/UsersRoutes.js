import read_users from "../../Controller/users/ReadController.js";
import read_dashboard from "../../Controller/users/ReadDashboardController.js";
import read_users_trial from "../../Controller/users/ReadTrialController.js";
import create_users_check from "../../Controller/users/CheckPasienController.js";
import create_users from "../../Controller/users/CreatePasienController.js";
import create_glucose from "../../Controller/users/CreateGlucoseController.js";
import read_print from "../../Controller/users/PrintController.js";
import read_glucose from "../../Controller/users/ReadDetailController.js";
import update_glucose from "../../Controller/users/DeleteGlucoseController.js";
import AuthMiddleware from "../../Middleware/authentication.js";

const usersRoutes = (app) => {
  app.route(`/api/v1/users/read`).get(read_users);
  app.route(`/api/v1/dashboard/read`).get(read_dashboard);
  app.route(`/api/v1/users_trial/read`).get(read_users_trial);
  app.route(`/api/v1/users_check/read`).get(create_users_check);
  app.route(`/api/v1/users/create`).post(create_users);
  app.route(`/api/v1/glucose/create`).post(create_glucose);
  app.route(`/api/v1/print/read`).get(read_print);
  app.route(`/api/v1/glucose/read`).get(read_glucose);
  app.route(`/api/v1/glucose/update`).put(update_glucose);
};
export { usersRoutes };
