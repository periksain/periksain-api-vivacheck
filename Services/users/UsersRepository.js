import users from "../../Model/users.js";
import users_child from "../../Model/users_child.js";
import glucose_check from "../../Model/glucose_check.js";
import { Op } from "sequelize";
import sequelize from "sequelize";
import { api_registrasi } from "../../Helper/api.js";
import sequelizeInstance from "../../Config/config.js";
import { crypt } from "../../Helper/Helper.js";

users.hasMany(users_child, { foreignKey: "users_id" });
users_child.belongsTo(users, { foreignKey: "users_id" });

users_child.hasMany(glucose_check, { foreignKey: "users_child_id" });
glucose_check.belongsTo(users_child, { foreignKey: "users_child_id" });

// Start Session Create Data users
const createusers = async (data, transaction) => {
  const t = transaction ? transaction : await users.sequelize.transaction();
  try {
    let result = await users.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] Createusers", error);
    throw new Error(error);
  }
};

const createusers_child = async (data, transaction) => {
  const t = transaction
    ? transaction
    : await users_child.sequelize.transaction();
  try {
    let result = await users_child.create(data, { transaction });
    let hasil = crypt("vivachek", "child "+result.id);
    result.code = hasil; // Set nilai code dengan hasil enkripsi
    await result.save({ transaction }); // Simpan hasil perubahan
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] Createusers_child", error);
    throw new Error(error);
  }
};

const createglucose_check = async (data, transaction) => {
  const t = transaction
    ? transaction
    : await glucose_check.sequelize.transaction();
  try {
    let result = await glucose_check.create(data, { transaction });
    let hasil = crypt("vivachek", "hasil "+result.id);
    result.code = hasil; // Set nilai code dengan hasil enkripsi
    await result.save({ transaction }); // Simpan hasil perubahan
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] Createglucose_check", error);
    throw new Error(error);
  }
};
// End Session Create Data users

// Start Session Update Data users
const updateusers = async (data, filter, transaction) => {
  const t = transaction ? transaction : await users.sequelize.transaction();
  try {
    let result = await users.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] updateusers", error);
    throw new Error(error);
  }
};

const updateusers_child = async (data, filter, transaction) => {
  const t = transaction ? transaction : await users_child.sequelize.transaction();
  try {
    let result = await users_child.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] updateusers_child", error);
    throw new Error(error);
  }
};

const updateglucose_check = async (data, filter, transaction) => {
  const t = transaction ? transaction : await glucose_check.sequelize.transaction();
  try {
    let result = await glucose_check.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] updateglucose_check", error);
    throw new Error(error);
  }
};
// End Session Update Data users

const readusersFirst = async (search, id, date, date2) => {
  try {
    let result = await users.findOne({
      where: {
        name: id,
      },
      // limit: 1,
      include: [
        {
          model: users_child,
          // order: [["id", "DESC"]],
          where: {
            // [Op.or]: [
            //   //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
            //   //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
            //   //   {
            //   //     code: sequelize.where(
            //   //       sequelize.fn("LOWER", sequelize.col("code")),
            //   //       "LIKE",
            //   //       "%" + search + "%"
            //   //     ),
            //   //   },
            //   {
            //     name: search
            //       ? sequelize.where(
            //           sequelize.fn("LOWER", sequelize.col("name")),
            //           "LIKE",
            //           "%" + search + "%"
            //         )
            //       : { [Op.like]: `%%` },
            //   },
            // ],
            id: search || { [Op.not]: null },
          },
          // limit: 1,
          include: [
            {
              model: glucose_check,
              where: {
                status: true,
                inspection_time_date: { [Op.between]: [date, date2] },
              },
            },
          ],
        },
      ],
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersFirst", error);
    throw new Error(error);
  }
};

const readusersFirstNoGlucose = async (search, id) => {
  try {
    let result = await users.findOne({
      where: {
        name: id,
      },
      // limit: 1,
      include: [
        {
          model: users_child,
          // order: [["id", "DESC"]],
          where: {
            // [Op.or]: [
            //   //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
            //   //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
            //   //   {
            //   //     code: sequelize.where(
            //   //       sequelize.fn("LOWER", sequelize.col("code")),
            //   //       "LIKE",
            //   //       "%" + search + "%"
            //   //     ),
            //   //   },
            //   {
            //     name: search
            //       ? sequelize.where(
            //           sequelize.fn("LOWER", sequelize.col("name")),
            //           "LIKE",
            //           "%" + search + "%"
            //         )
            //       : { [Op.like]: `%%` },
            //   },
            // ],
            id: search || { [Op.not]: null },
          },
          // limit: 1,
          // include: [
          //   {
          //     model: glucose_check,
          //     where: {
          //       inspection_time_date: { [Op.between]: [date, date2] },
          //     },
          //   },
          // ],
        },
      ],
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersFirstNoGlucose", error);
    throw new Error(error);
  }
};

const readusersFirstOnly = async (id) => {
  try {
    let result = await users.findAndCountAll({
      where: {
        name: id,
      },
      include: [
        {
          model: users_child,
        },
      ],
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersFirstOnly", error);
    throw new Error(error);
  }
};

const readusers = async (search, page, page_size, lab, rs) => {
  try {
    let result = await users.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          //   {
          //     code: sequelize.where(
          //       sequelize.fn("LOWER", sequelize.col("code")),
          //       "LIKE",
          //       "%" + search + "%"
          //     ),
          //   },
          {
            name: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("name")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      offset: page_size * page,
      limit: page_size,
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusers", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readuserslistGrafik = async (alat, nolot, control, lab, rs) => {
  try {
    let result = await users.findAndCountAll({
      where: {
        [Op.and]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          nolot && { nomor: nolot },
          control && { nama_control: control },
          alat && { nama_alat: alat },
          // nolot && {nomor: sequelize.where(sequelize.fn('LOWER', sequelize.col('nomor')), 'LIKE', '%' + nolot + '%')},
          // control && {nama_control: sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_control')), 'LIKE', '%' + control + '%')},
          // alat && {nama_alat: sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_alat')), 'LIKE', '%' + alat + '%')},
          // {nomor: nolot ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nomor')), 'LIKE', '%' + nolot + '%') : { [Op.like]: `%%` }},
          // {nama_control: control ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_control')), 'LIKE', '%' + control + '%') : { [Op.like]: `%%` }},
          // {nama_alat: alat ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_alat')), 'LIKE', '%' + alat + '%') : { [Op.like]: `%%` }},
        ],
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readuserslistGrafik", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readusersAll = async (search, lab, rs) => {
  try {
    let result = await users.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          //   {
          //     code: sequelize.where(
          //       sequelize.fn("LOWER", sequelize.col("code")),
          //       "LIKE",
          //       "%" + search + "%"
          //     ),
          //   },
          {
            name: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("name")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        kode_lab: lab,
        kode_rs: rs,
        status: true,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersAll", error);
    throw new Error(error);
  }
};
// End Session Read Data users

const readusersAllHampirExpired = async (lab, rs) => {
  try {
    var todays = new Date();
    console.log("satu 1 ", todays);
    todays.setDate(todays.getDate() + 1);
    console.log("satu 2 ", todays);
    var bulan =
      todays.getMonth() + 1 < 10
        ? "0" + (todays.getMonth() + 1)
        : todays.getMonth() + 1;
    var tanggal =
      todays.getDate() < 10 ? "0" + todays.getDate() : todays.getDate();
    var newToday = todays.getFullYear() + "-" + bulan + "-" + tanggal;
    console.log("satu 3 ", newToday);
    newToday = newToday + "T00:00:00.000Z";
    console.log("satu 4 ", newToday);
    let result = await users.findAll({
      where: {
        waktu_habis: newToday,
        kode_lab: lab,
        kode_rs: rs,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersAllHampirExpired", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readusersaktif = async ({ nousers }, page, page_size, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await users.findAndCountAll({
      where: {
        no_users: nousers
          ? { [Op.iLike]: `%${nousers}%` }
          : { [Op.iLike]: `%%` },
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersaktif", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readusersalllast = async (koders, kodelab) => {
  try {
    // console.log("tes",klinik);
    let result = await users.findAll({
      where: {
        kode_rs: koders,
        kode_lab: kodelab,
      },
      limit: 1,
      order: [["created_at", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersalllast", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readusersnorm = async (norm, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await users.findAndCountAll({
      where: {
        no_rm: norm,
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersnorm", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readusersrslab = async (norm, rs, lab) => {
  try {
    // console.log("tes",klinik);
    let result = await users.findOne({
      where: {
        no_users: norm,
        kode_rs: rs,
        kode_lab: lab,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersrslab", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users
const readusersnoreg = async (noreg, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await users.findAndCountAll({
      where: {
        no_regrs: noreg,
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersnoreg", error);
    throw new Error(error);
  }
};
// End Session Read Data users

// Start Session Read Data users With Status = True
const readusersStatusAktif = async ({ nama }, klinik) => {
  try {
    let result = await users.findAndCountAll({
      where: {
        nama_pasien: nama ? { [Op.iLike]: `%${nama}%` } : { [Op.iLike]: `%%` },
        clinic_id: klinik,
        status: true,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersStatusAktif", error);
    throw new Error(error);
  }
};
// End Session Read Data users With Status = True

// Start Session Read users By Id
const readusersById = async (id) => {
  try {
    let result = await users.findByPk(id);
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readusersById", error);
    throw new Error(error);
  }
};
// End Session Read users By Id

const findOne_users = async (filter) => {
  try {
    let result = await users.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findOne_users", error);
    throw new Error(error);
  }
};

const findOne_glucose_check = async (filter) => {
  try {
    let result = await glucose_check.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findOne_glucose_check", error);
    throw new Error(error);
  }
};

const deleteglucose_check = async (filter, transaction) => {
  const t = transaction
    ? transaction
    : await glucose_check.sequelize.transaction();
  try {
    let result = await glucose_check.destroy({
      ...filter,
      transaction,
    });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] deleteglucose_check", error);
    throw new Error(error);
  }
};

const findOne_users_child = async (filter) => {
  try {
    let result = await users_child.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findOne_users_child", error);
    throw new Error(error);
  }
};

const readAllusers = async (filter) => {
  try {
    let result = await users.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllusers_child = async (filter) => {
  try {
    let result = await users_child.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllusers_child", error);
    throw new Error(error);
  }
};

const readAllglucose_check = async (filter) => {
  try {
    let result = await glucose_check.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllglucose_check", error);
    throw new Error(error);
  }
};

const readAllnousers = async (no_users, alat, koders, kodelab) => {
  try {
    let result = await users.findAll({
      where: {
        nomor: no_users,
        nama_alat: alat,
        kode_rs: koders,
        kode_lab: kodelab,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllnousers", error);
    throw new Error(error);
  }
};

const readLotByIdAlat = async (t_alat_qc_id) => {
  try {
    let result = await users.findOne({
      where: {
        t_alat_qc_id: t_alat_qc_id,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readLotByIdAlat", error);
    throw new Error(error);
  }
};

// Start Session users By Kode
const findusersByKode = async (kode) => {
  try {
    let result = await users.findOne({
      where: {
        kode: { [Op.iLike]: `${kode}` },
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findusersByKode", error);
    throw new Error(error);
  }
};

const readAllusersByKode = async (kode) => {
  try {
    let result = await users.findAll({
      where: {
        kode: { [Op.iLike]: `${kode}` },
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllusersByKode", error);
    throw new Error(error);
  }
};
// End Session users By Kode

// Start Session users By Nama
const findusersByNama = async (nama) => {
  try {
    let result = await users.findOne({
      where: {
        nama_pasien: { [Op.iLike]: `${nama}` },
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllusersByNama = async (nama) => {
  try {
    let result = await users.findAll({
      where: {
        nama_pasien: { [Op.iLike]: `${nama}` },
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
// End Session users By Nama

// Start Session users By Tanggal Pemasangan
const findusersByTanggalPemasangan = async (tanggal_pemasangan) => {
  try {
    let result = await users.findOne({
      where: {
        tanggal_pemasangan: tanggal_pemasangan,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findusersByTanggalPemasangan", error);
    throw new Error(error);
  }
};

const readAllusersByTanggalPemasangan = async (tanggal_pemasangan) => {
  try {
    let result = await users.findAll({
      where: {
        tanggal_pemasangan: tanggal_pemasangan,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllusersByTanggalPemasangan", error);
    throw new Error(error);
  }
};
// End Session users By Tanggal Pemasangan

// Start Session users By Id Klinik
const findusersByIdKlinik = async (clinic_id) => {
  try {
    let result = await users.findOne({
      where: {
        clinic_id: clinic_id,
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllusersByIdKlinik = async (clinic_id) => {
  try {
    let result = await users.findAll({
      where: {
        clinic_id: clinic_id,
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
// End Session users By Id Klinik

// Start Session Filter
const readByFilter = async (filter) => {
  try {
    const filterData = {};

    if (filter.clinic_id) {
      filterData.clinic_id = filter.clinic_id; //Filtered By Id Klinik
    }

    if (filter.nama) {
      filterData.nama = filter.nama; //Filtered By Name
    }

    if (filter.kode) {
      filterData.kode = filter.kode; // Filtered By Code
    }

    // Find Data users Based On Filters
    let result = await users.findOne({
      where: filterData,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readByFilter", error);
    throw new Error(error);
  }
};
// End Session Filter

export {
  createusers,
  createusers_child,
  createglucose_check,
  updateusers,
  updateusers_child,
  readusers,
  readusersStatusAktif,
  readusersById,
  readAllusersByNama,
  findusersByNama,
  readAllusersByTanggalPemasangan,
  findusersByTanggalPemasangan,
  findusersByKode,
  readAllusersByKode,
  readAllusersByIdKlinik,
  findusersByIdKlinik,
  readAllusers,
  readByFilter,
  readAllnousers,
  readLotByIdAlat,
  readusersnoreg,
  readusersaktif,
  readusersnorm,
  readusersAll,
  readAllusers_child,
  readAllglucose_check,
  findOne_users,
  readuserslistGrafik,
  readusersAllHampirExpired,
  readusersalllast,
  readusersrslab,
  readusersFirst,
  findOne_users_child,
  readusersFirstOnly,
  readusersFirstNoGlucose,
  findOne_glucose_check,
  deleteglucose_check,
  updateglucose_check
};
