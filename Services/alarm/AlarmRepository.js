import alarm from "../../Model/alarm.js";
import users from "../../Model/users.js";
import users_child from "../../Model/users_child.js";
import { Op } from "sequelize";
import sequelize from "sequelize";
import axios from "axios";
import { api_registrasi } from "../../Helper/api.js";
import sequelizeInstance from "../../Config/config.js";
import { crypt } from "../../Helper/Helper.js";

users.hasMany(users_child, { foreignKey: "users_id" });
users_child.belongsTo(users, { foreignKey: "users_id" });

users_child.hasMany(alarm, { foreignKey: "users_child_id" });
alarm.belongsTo(users_child, { foreignKey: "users_child_id" });


// Start Session Create Data alarm
const createalarm = async (data, transaction) => {
  const t = transaction ? transaction : await alarm.sequelize.transaction();
  try {
    let result = await alarm.create(data, { transaction });
    let hasil = crypt("vivachek", "alarm "+result.id);
    result.code = hasil; // Set nilai code dengan hasil enkripsi
    await result.save({ transaction }); // Simpan hasil perubahan
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] Createalarm", error);
    throw new Error(error);
  }
};
// End Session Create Data alarm

const deletealarm = async (filter, transaction) => {
  const t = transaction
    ? transaction
    : await alarm.sequelize.transaction();
  try {
    let result = await alarm.destroy({
      ...filter,
      transaction,
    });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] deletealarm", error);
    throw new Error(error);
  }
};

// Start Session Update Data alarm
const updatealarm = async (data, filter, transaction) => {
  const t = transaction ? transaction : await alarm.sequelize.transaction();
  try {
    let result = await alarm.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] updatealarm", error);
    throw new Error(error);
  }
};
// End Session Update Data alarm

// Start Session Read Data alarm
const readalarmdate = async (search, page, page_size, lab, rs, dari, sampai) => {
  try {
    let result = await alarm.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          //   {no_alarm: sequelize.where(sequelize.fn('LOWER', sequelize.col('no_alarm')), 'LIKE', '%' + search + '%')},
          {
            nama: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("nama")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        waktu_alarm:
          dari || sampai
            ? {
                [Op.between]: [dari, sampai],
              }
            : { [Op.not]: null },
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      offset: page_size * page,
      limit: page_size,
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmdate", error);
    throw new Error(error);
  }
};
// Start Session Read Data alarm

const readalarm = async (id) => {
  try {
    let result = await users.findOne({
      where: {
        name: id
      },
      include: [
        {
          model: users_child,
          include: [
            {
              model: alarm,
            },
          ],
        },
      ],
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarm", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmlistGrafik = async (alat, nolot, control, lab, rs) => {
  try {
    let result = await alarm.findAndCountAll({
      where: {
        [Op.and]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          nolot && { nomor: nolot },
          control && { nama_control: control },
          alat && { nama_alat: alat },
          // nolot && {nomor: sequelize.where(sequelize.fn('LOWER', sequelize.col('nomor')), 'LIKE', '%' + nolot + '%')},
          // control && {nama_control: sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_control')), 'LIKE', '%' + control + '%')},
          // alat && {nama_alat: sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_alat')), 'LIKE', '%' + alat + '%')},
          // {nomor: nolot ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nomor')), 'LIKE', '%' + nolot + '%') : { [Op.like]: `%%` }},
          // {nama_control: control ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_control')), 'LIKE', '%' + control + '%') : { [Op.like]: `%%` }},
          // {nama_alat: alat ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_alat')), 'LIKE', '%' + alat + '%') : { [Op.like]: `%%` }},
        ],
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmlistGrafik", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmAll = async (search, lab, rs) => {
  try {
    let result = await alarm.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
        //   {
        //     code: sequelize.where(
        //       sequelize.fn("LOWER", sequelize.col("code")),
        //       "LIKE",
        //       "%" + search + "%"
        //     ),
        //   },
          {
            name: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("name")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        kode_lab: lab,
        kode_rs: rs,
        status: true,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmAll", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

const readalarmAllHampirExpired = async (lab, rs) => {
  try {
    var todays = new Date();
    console.log("satu 1 ", todays);
    todays.setDate(todays.getDate() + 1);
    console.log("satu 2 ", todays);
    var bulan =
      todays.getMonth() + 1 < 10
        ? "0" + (todays.getMonth() + 1)
        : todays.getMonth() + 1;
    var tanggal =
      todays.getDate() < 10 ? "0" + todays.getDate() : todays.getDate();
    var newToday = todays.getFullYear() + "-" + bulan + "-" + tanggal;
    console.log("satu 3 ", newToday);
    newToday = newToday + "T00:00:00.000Z";
    console.log("satu 4 ", newToday);
    let result = await alarm.findAll({
      where: {
        waktu_habis: newToday,
        kode_lab: lab,
        kode_rs: rs,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmAllHampirExpired", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmaktif = async ({ noalarm }, page, page_size, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await alarm.findAndCountAll({
      where: {
        no_alarm: noalarm ? { [Op.iLike]: `%${noalarm}%` } : { [Op.iLike]: `%%` },
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmaktif", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmalllast = async (koders, kodelab) => {
  try {
    // console.log("tes",klinik);
    let result = await alarm.findAll({
      where: {
        kode_rs: koders,
        kode_lab: kodelab,
      },
      limit: 1,
      order: [["created_at", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmalllast", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmnorm = async (norm, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await alarm.findAndCountAll({
      where: {
        no_rm: norm,
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmnorm", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmrslab = async (norm, rs, lab) => {
  try {
    // console.log("tes",klinik);
    let result = await alarm.findOne({
      where: {
        no_alarm: norm,
        kode_rs: rs,
        kode_lab: lab,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmrslab", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm
const readalarmnoreg = async (noreg, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await alarm.findAndCountAll({
      where: {
        no_regrs: noreg,
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmnoreg", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm

// Start Session Read Data alarm With Status = True
const readalarmStatusAktif = async ({ nama }, klinik) => {
  try {
    let result = await alarm.findAndCountAll({
      where: {
        nama_pasien: nama ? { [Op.iLike]: `%${nama}%` } : { [Op.iLike]: `%%` },
        clinic_id: klinik,
        status: true,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmStatusAktif", error);
    throw new Error(error);
  }
};
// End Session Read Data alarm With Status = True

// Start Session Read alarm By Id
const readalarmById = async (id) => {
  try {
    let result = await alarm.findByPk(id);
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readalarmById", error);
    throw new Error(error);
  }
};
// End Session Read alarm By Id

const findOne_alarm = async (filter) => {
  try {
    let result = await alarm.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findOne_alarm", error);
    throw new Error(error);
  }
};

const readAllalarm = async (filter) => {
  try {
    let result = await alarm.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllnoalarm = async (no_alarm, alat, koders, kodelab) => {
  try {
    let result = await alarm.findAll({
      where: {
        nomor: no_alarm,
        nama_alat: alat,
        kode_rs: koders,
        kode_lab: kodelab,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllnoalarm", error);
    throw new Error(error);
  }
};

const readLotByIdAlat = async (t_alat_qc_id) => {
  try {
    let result = await alarm.findOne({
      where: {
        t_alat_qc_id: t_alat_qc_id,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readLotByIdAlat", error);
    throw new Error(error);
  }
};

// Start Session alarm By Kode
const findalarmByKode = async (kode) => {
  try {
    let result = await alarm.findOne({
      where: {
        kode: { [Op.iLike]: `${kode}` },
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findalarmByKode", error);
    throw new Error(error);
  }
};

const readAllalarmByKode = async (kode) => {
  try {
    let result = await alarm.findAll({
      where: {
        kode: { [Op.iLike]: `${kode}` },
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllalarmByKode", error);
    throw new Error(error);
  }
};
// End Session alarm By Kode

// Start Session alarm By Nama
const findalarmByNama = async (nama) => {
  try {
    let result = await alarm.findOne({
      where: {
        nama_pasien: { [Op.iLike]: `${nama}` },
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllalarmByNama = async (nama) => {
  try {
    let result = await alarm.findAll({
      where: {
        nama_pasien: { [Op.iLike]: `${nama}` },
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
// End Session alarm By Nama

// Start Session alarm By Tanggal Pemasangan
const findalarmByTanggalPemasangan = async (tanggal_pemasangan) => {
  try {
    let result = await alarm.findOne({
      where: {
        tanggal_pemasangan: tanggal_pemasangan,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findalarmByTanggalPemasangan", error);
    throw new Error(error);
  }
};

const readAllalarmByTanggalPemasangan = async (tanggal_pemasangan) => {
  try {
    let result = await alarm.findAll({
      where: {
        tanggal_pemasangan: tanggal_pemasangan,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllalarmByTanggalPemasangan", error);
    throw new Error(error);
  }
};
// End Session alarm By Tanggal Pemasangan

// Start Session alarm By Id Klinik
const findalarmByIdKlinik = async (clinic_id) => {
  try {
    let result = await alarm.findOne({
      where: {
        clinic_id: clinic_id,
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllalarmByIdKlinik = async (clinic_id) => {
  try {
    let result = await alarm.findAll({
      where: {
        clinic_id: clinic_id,
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
// End Session alarm By Id Klinik

// Start Session Filter
const readByFilter = async (filter) => {
  try {
    const filterData = {};

    if (filter.clinic_id) {
      filterData.clinic_id = filter.clinic_id; //Filtered By Id Klinik
    }

    if (filter.nama) {
      filterData.nama = filter.nama; //Filtered By Name
    }

    if (filter.kode) {
      filterData.kode = filter.kode; // Filtered By Code
    }

    // Find Data alarm Based On Filters
    let result = await alarm.findOne({
      where: filterData,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readByFilter", error);
    throw new Error(error);
  }
};
// End Session Filter

export {
  createalarm,
  updatealarm,
  readalarm,
  readalarmStatusAktif,
  readalarmById,
  readAllalarmByNama,
  findalarmByNama,
  readAllalarmByTanggalPemasangan,
  findalarmByTanggalPemasangan,
  findalarmByKode,
  readAllalarmByKode,
  readAllalarmByIdKlinik,
  findalarmByIdKlinik,
  readAllalarm,
  readByFilter,
  readAllnoalarm,
  readLotByIdAlat,
  readalarmnoreg,
  readalarmaktif,
  readalarmnorm,
  readalarmAll,
  findOne_alarm,
  readalarmlistGrafik,
  readalarmAllHampirExpired,
  readalarmalllast,
  readalarmrslab,
  deletealarm
};
