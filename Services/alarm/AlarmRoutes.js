import read_users from "../../Controller/Alarm/ReadController.js";
import read_users_id from "../../Controller/Alarm/ReadDetailController.js";
import create_alarm from "../../Controller/Alarm/CreateController.js";
import update_alarm from "../../Controller/Alarm/UpdateController.js";
import delete_alarm from "../../Controller/Alarm/DeleteController.js";
import AuthMiddleware from "../../Middleware/authentication.js";

const alarmRoutes = (app) => {
  app.route(`/api/v1/alarm/read`).get(read_users);
  app.route(`/api/v1/alarm_detail/read`).get(read_users_id);
  app.route(`/api/v1/alarm/create`).post(create_alarm);
  app.route(`/api/v1/alarm/update`).put(update_alarm);
  app.route(`/api/v1/alarm_delete/update`).put(delete_alarm);
//   app.route(`/api/v1/glucose/create`).post(create_glucose);
};
export { alarmRoutes };
