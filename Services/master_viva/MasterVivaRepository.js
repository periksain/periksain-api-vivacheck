import master_viva from "../../Model/master_viva.js";
import { Op } from "sequelize";
import sequelize from "sequelize";
import axios from "axios";
import { api_registrasi } from "../../Helper/api.js";
import sequelizeInstance from "../../Config/config.js";

// Start Session Create Data master_viva
const createmaster_viva = async (data, transaction) => {
  const t = transaction
    ? transaction
    : await master_viva.sequelize.transaction();
  try {
    let result = await master_viva.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] Createmaster_viva", error);
    throw new Error(error);
  }
};

const createmaster_vivabulk = async (data, transaction) => {
  const t = transaction
    ? transaction
    : await master_viva.sequelize.transaction();
  try {
    let result = await master_viva.bulkCreate(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] Createmaster_vivabulk", error);
    throw new Error(error);
  }
};
// End Session Create Data master_viva

// Start Session Update Data master_viva
const updatemaster_viva = async (data, filter, transaction) => {
  const t = transaction
    ? transaction
    : await master_viva.sequelize.transaction();
  try {
    let result = await master_viva.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error("[EXCEPTION] updatemaster_viva", error);
    throw new Error(error);
  }
};
// End Session Update Data master_viva

// Start Session Read Data master_viva
const readmaster_vivadate = async (
  search,
  page,
  page_size,
  lab,
  rs,
  dari,
  sampai
) => {
  try {
    let result = await master_viva.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          //   {no_master_viva: sequelize.where(sequelize.fn('LOWER', sequelize.col('no_master_viva')), 'LIKE', '%' + search + '%')},
          {
            nama: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("nama")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        waktu_master_viva:
          dari || sampai
            ? {
                [Op.between]: [dari, sampai],
              }
            : { [Op.not]: null },
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      offset: page_size * page,
      limit: page_size,
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivadate", error);
    throw new Error(error);
  }
};
// Start Session Read Data master_viva

const readmaster_viva = async (search, page, page_size, lab, rs) => {
  try {
    let result = await master_viva.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          //   {
          //     code: sequelize.where(
          //       sequelize.fn("LOWER", sequelize.col("code")),
          //       "LIKE",
          //       "%" + search + "%"
          //     ),
          //   },
          {
            name: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("name")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      offset: page_size * page,
      limit: page_size,
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_viva", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivalistGrafik = async (alat, nolot, control, lab, rs) => {
  try {
    let result = await master_viva.findAndCountAll({
      where: {
        [Op.and]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          nolot && { nomor: nolot },
          control && { nama_control: control },
          alat && { nama_alat: alat },
          // nolot && {nomor: sequelize.where(sequelize.fn('LOWER', sequelize.col('nomor')), 'LIKE', '%' + nolot + '%')},
          // control && {nama_control: sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_control')), 'LIKE', '%' + control + '%')},
          // alat && {nama_alat: sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_alat')), 'LIKE', '%' + alat + '%')},
          // {nomor: nolot ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nomor')), 'LIKE', '%' + nolot + '%') : { [Op.like]: `%%` }},
          // {nama_control: control ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_control')), 'LIKE', '%' + control + '%') : { [Op.like]: `%%` }},
          // {nama_alat: alat ? sequelize.where(sequelize.fn('LOWER', sequelize.col('nama_alat')), 'LIKE', '%' + alat + '%') : { [Op.like]: `%%` }},
        ],
        kode_lab: lab ? lab : { [Op.iLike]: `%%` },
        kode_rs: rs ? rs : { [Op.iLike]: `%%` },
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivalistGrafik", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivaAll = async (search, lab, rs) => {
  try {
    let result = await master_viva.findAndCountAll({
      where: {
        [Op.or]: [
          //   nama_pasien: search ? { [Op.iLike]: `%${search}%` } : { [Op.iLike]: `%%` },
          //   {url: sequelize.where(sequelize.fn('LOWER', sequelize.col('url')), 'LIKE', '%' + search + '%')},
          //   {
          //     code: sequelize.where(
          //       sequelize.fn("LOWER", sequelize.col("code")),
          //       "LIKE",
          //       "%" + search + "%"
          //     ),
          //   },
          {
            name: search
              ? sequelize.where(
                  sequelize.fn("LOWER", sequelize.col("name")),
                  "LIKE",
                  "%" + search + "%"
                )
              : { [Op.like]: `%%` },
          },
        ],
        kode_lab: lab,
        kode_rs: rs,
        status: true,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivaAll", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

const readmaster_vivaAllHampirExpired = async (lab, rs) => {
  try {
    var todays = new Date();
    console.log("satu 1 ", todays);
    todays.setDate(todays.getDate() + 1);
    console.log("satu 2 ", todays);
    var bulan =
      todays.getMonth() + 1 < 10
        ? "0" + (todays.getMonth() + 1)
        : todays.getMonth() + 1;
    var tanggal =
      todays.getDate() < 10 ? "0" + todays.getDate() : todays.getDate();
    var newToday = todays.getFullYear() + "-" + bulan + "-" + tanggal;
    console.log("satu 3 ", newToday);
    newToday = newToday + "T00:00:00.000Z";
    console.log("satu 4 ", newToday);
    let result = await master_viva.findAll({
      where: {
        waktu_habis: newToday,
        kode_lab: lab,
        kode_rs: rs,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivaAllHampirExpired", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivaaktif = async (
  { nomaster_viva },
  page,
  page_size,
  klinik
) => {
  try {
    // console.log("tes",klinik);
    let result = await master_viva.findAndCountAll({
      where: {
        no_master_viva: nomaster_viva
          ? { [Op.iLike]: `%${nomaster_viva}%` }
          : { [Op.iLike]: `%%` },
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivaaktif", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivaalllast = async (koders, kodelab) => {
  try {
    // console.log("tes",klinik);
    let result = await master_viva.findAll({
      where: {
        kode_rs: koders,
        kode_lab: kodelab,
      },
      limit: 1,
      order: [["created_at", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivaalllast", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivanorm = async (norm, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await master_viva.findAndCountAll({
      where: {
        no_rm: norm,
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivanorm", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivarslab = async (norm, rs, lab) => {
  try {
    // console.log("tes",klinik);
    let result = await master_viva.findOne({
      where: {
        no_master_viva: norm,
        kode_rs: rs,
        kode_lab: lab,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivarslab", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva
const readmaster_vivanoreg = async (noreg, klinik) => {
  try {
    // console.log("tes",klinik);
    let result = await master_viva.findAndCountAll({
      where: {
        no_regrs: noreg,
        clinic_id: klinik,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivanoreg", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva

// Start Session Read Data master_viva With Status = True
const readmaster_vivaStatusAktif = async ({ nama }, klinik) => {
  try {
    let result = await master_viva.findAndCountAll({
      where: {
        nama_pasien: nama ? { [Op.iLike]: `%${nama}%` } : { [Op.iLike]: `%%` },
        clinic_id: klinik,
        status: true,
      },
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivaStatusAktif", error);
    throw new Error(error);
  }
};
// End Session Read Data master_viva With Status = True

// Start Session Read master_viva By Id
const readmaster_vivaById = async (id) => {
  try {
    let result = await master_viva.findByPk(id);
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readmaster_vivaById", error);
    throw new Error(error);
  }
};
// End Session Read master_viva By Id

const findOne_master_viva = async (filter) => {
  try {
    let result = await master_viva.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findOne_master_viva", error);
    throw new Error(error);
  }
};

const readAllmaster_viva = async (filter) => {
  try {
    let result = await master_viva.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllmaster_viva_check = async () => {
  try {
    let result = await master_viva.findAll({
      attributes: ['code'],
      group: ['code'],
      having: sequelize.literal('COUNT(code) > 1'),
      where:{
        status:true
      }
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllnomaster_viva = async (no_master_viva, alat, koders, kodelab) => {
  try {
    let result = await master_viva.findAll({
      where: {
        nomor: no_master_viva,
        nama_alat: alat,
        kode_rs: koders,
        kode_lab: kodelab,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllnomaster_viva", error);
    throw new Error(error);
  }
};

const readLotByIdAlat = async (t_alat_qc_id) => {
  try {
    let result = await master_viva.findOne({
      where: {
        t_alat_qc_id: t_alat_qc_id,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readLotByIdAlat", error);
    throw new Error(error);
  }
};

// Start Session master_viva By Kode
const findmaster_vivaByKode = async (kode) => {
  try {
    let result = await master_viva.findOne({
      where: {
        kode: { [Op.iLike]: `${kode}` },
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findmaster_vivaByKode", error);
    throw new Error(error);
  }
};

const readAllmaster_vivaByKode = async (kode) => {
  try {
    let result = await master_viva.findAll({
      where: {
        kode: { [Op.iLike]: `${kode}` },
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllmaster_vivaByKode", error);
    throw new Error(error);
  }
};
// End Session master_viva By Kode

// Start Session master_viva By Nama
const findmaster_vivaByNama = async (nama) => {
  try {
    let result = await master_viva.findOne({
      where: {
        nama_pasien: { [Op.iLike]: `${nama}` },
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllmaster_vivaByNama = async (nama) => {
  try {
    let result = await master_viva.findAll({
      where: {
        nama_pasien: { [Op.iLike]: `${nama}` },
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
// End Session master_viva By Nama

// Start Session master_viva By Tanggal Pemasangan
const findmaster_vivaByTanggalPemasangan = async (tanggal_pemasangan) => {
  try {
    let result = await master_viva.findOne({
      where: {
        tanggal_pemasangan: tanggal_pemasangan,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] findmaster_vivaByTanggalPemasangan", error);
    throw new Error(error);
  }
};

const readAllmaster_vivaByTanggalPemasangan = async (tanggal_pemasangan) => {
  try {
    let result = await master_viva.findAll({
      where: {
        tanggal_pemasangan: tanggal_pemasangan,
      },
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readAllmaster_vivaByTanggalPemasangan", error);
    throw new Error(error);
  }
};
// End Session master_viva By Tanggal Pemasangan

// Start Session master_viva By Id Klinik
const findmaster_vivaByIdKlinik = async (clinic_id) => {
  try {
    let result = await master_viva.findOne({
      where: {
        clinic_id: clinic_id,
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const readAllmaster_vivaByIdKlinik = async (clinic_id) => {
  try {
    let result = await master_viva.findAll({
      where: {
        clinic_id: clinic_id,
      },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
// End Session master_viva By Id Klinik

// Start Session Filter
const readByFilter = async (filter) => {
  try {
    const filterData = {};

    if (filter.clinic_id) {
      filterData.clinic_id = filter.clinic_id; //Filtered By Id Klinik
    }

    if (filter.nama) {
      filterData.nama = filter.nama; //Filtered By Name
    }

    if (filter.kode) {
      filterData.kode = filter.kode; // Filtered By Code
    }

    // Find Data master_viva Based On Filters
    let result = await master_viva.findOne({
      where: filterData,
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] readByFilter", error);
    throw new Error(error);
  }
};
// End Session Filter

const getLastMaster_viva = async () => {
  try {
    let result = await master_viva.findAll({
      where: {
        code: { [Op.startsWith]: "VIVA" },
        status: true
      },
      limit: 1,
      order: [["id", "DESC"]],
    });
    return result;
  } catch (error) {
    console.error("[EXCEPTION] getLastMaster_viva", error);
    throw new Error(error);
  }
};

export {
  createmaster_viva,
  createmaster_vivabulk,
  updatemaster_viva,
  readmaster_viva,
  readmaster_vivaStatusAktif,
  readmaster_vivaById,
  readAllmaster_vivaByNama,
  findmaster_vivaByNama,
  readAllmaster_vivaByTanggalPemasangan,
  findmaster_vivaByTanggalPemasangan,
  findmaster_vivaByKode,
  readAllmaster_vivaByKode,
  readAllmaster_vivaByIdKlinik,
  findmaster_vivaByIdKlinik,
  readAllmaster_viva,
  readByFilter,
  readAllnomaster_viva,
  readLotByIdAlat,
  readmaster_vivanoreg,
  readmaster_vivaaktif,
  readmaster_vivanorm,
  readmaster_vivaAll,
  findOne_master_viva,
  readmaster_vivalistGrafik,
  readmaster_vivaAllHampirExpired,
  readmaster_vivaalllast,
  readmaster_vivarslab,
  getLastMaster_viva,
  readAllmaster_viva_check
};
