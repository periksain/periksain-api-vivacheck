import create_viva from "../../Controller/master_viva/CreateController.js";
import read_viva from "../../Controller/master_viva/ReadController.js";
import read_viva_check from "../../Controller/master_viva/CheckController.js";
import AuthMiddleware from "../../Middleware/authentication.js";

const vivaRoutes = (app) => {
  app.route(`/api/v1/viva/create`).post(create_viva);
  app.route(`/api/v1/viva_check/create`).post(read_viva);
  app.route(`/api/v1/viva_check_duplicate/read`).get(read_viva_check);
};
export { vivaRoutes };
